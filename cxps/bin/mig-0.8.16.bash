# The Oracle programs have been updated for tnsnames.ora
# Hence we need to clean up any leftover oracle tgz
cd $CXPS_HOME/.cxps-src
\rm -f oracle*.tgz

if [ ! -z "$ORACLE_HOME" ] ; then
	cd $ORACLE_HOME
	if [ ! -d network -a -d admin ] ; then
		mkdir network
		mv admin network
	fi
fi
