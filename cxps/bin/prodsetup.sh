#!/usr/bin/env bash

# This program creates the directory structure for a given product that includes -
# - creation of workspace folder
# - source checkout from SVN_URL and given PRODUCT/VERSION
# - rt-config setup
# - deployment folder setup (copy TOMCAT, openfire, etc.)

. cxpsvars.sh	 # Assuming path is set

# -------------------
# MAIN ACTION BLOCK
# -------------------
if [ $# -ne 1 -a $# -ne 2 -a $# -ne 3 -a $# -ne 4 ] ; then
	echo "Usage: `basename $0` <PRODUCT> [VERSION] [SVN REVISION] [MODULE DIR]"
	exit 1
fi

PRODUCT=$1
VERSION=$2
REVISION=$3
MODDIR=$4

if [ -z "$VERSION" -a -z "${REVISION}" ] ; then
	rev=""
	SVN_PRODVER=$PRODUCT
	PRODVER=$PRODUCT
elif [ -z "$REVISION" ] ; then
	rev=""
	SVN_PRODVER=${PRODUCT}-${VERSION}
	PRODVER=${PRODUCT}-${VERSION}
elif [ -z "$VERSION" ] ; then
	rev="-r$REVISION"
	SVN_PRODVER=${PRODUCT}
	PRODVER=${PRODUCT}-r${REVISION}
else
	rev="-r$REVISION"
	SVN_PRODVER=${PRODUCT}-${VERSION}
	PRODVER=${PRODUCT}-${VERSION}-r${REVISION}
fi

# Setup the SVN Module Directory
[ ! -z "$MODDIR" ] && SVN_PRODVER=${MODDIR}

# Confirm from the user in case it already exist
if [ -d ${CXPS_DEV}/${PRODVER} ] ; then
	echo "WARNING: ${CXPS_DEV}/${PRODVER} already exists"
	echo "Press <Y> to continue overwriting it"
	read ans
	[ "$ans" != "Y" ] && echo "Exiting..." && exit 0
fi

# Create the Product Version folder
printf "Creating folders ..."
cd ${CXPS_DEV}
mkdir -p ${PRODVER} 2>/dev/null
mkdir -p ${PRODVER}/workspace 2>/dev/null
echo "done!"

# Checkout or update the source code
if [ -d ${PRODVER}/workspace ] ; then
	cd ${PRODVER}/workspace
	svn info >/dev/null 2>/dev/null
	if [ $? -ne 0 ] ; then
		echo "Source checkout ... "
		cd ..
		echo "svn $rev co ${GLB_SVN_URL}/${SVN_PRODVER} workspace"
		svn $rev co ${GLB_SVN_URL}/${SVN_PRODVER} workspace
	else
		echo "Source update ... "
		svn up
	fi
else
	echo "Full Source checkout ... "
	# echo svn $rev co ${GLB_SVN_URL}/${SVN_PRODVER} ${PRODVER}/workspace
	svn $rev co ${GLB_SVN_URL}/${SVN_PRODVER} ${PRODVER}/workspace
fi
