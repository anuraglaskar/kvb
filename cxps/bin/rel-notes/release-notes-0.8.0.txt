***** Release Notes for 0.8.0 ******

-------------
New Feature:
-------------

Module Support

1. The Subversion repository will contain folders with module structures
	- baseline => points to the trunk
	- releases => points to READ-ONLY tags
		- these are always version #s viz 4.2, 4.3, 5.0, etc.
		- these are always read-only.
	- branches => points to various branches taken from trunk
		- these can be any name

2. sw() will mark modules with suffix "[...]"
	- The existing branches remain unchanged
	- On choosing the module, it opens up a new option set
		- list of baseline and all tags / branches
	- rest of the behavior is retained

3. In SVN the module is contains prefix as 'mod_' prefix for identification

4. Moved few utilities to python
