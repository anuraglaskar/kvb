#!/bin/bash

#Create DB based on the DB name 
createDb(){

	DB=$1
	PASS=""
	USER="-uroot"

	# Get Root password
	if [ $# -gt 0 ];
	then
		PASS=$1
		if [ $PASS = 'None' ];
		then
			PASS=""
		fi
	else
		printf "Enter password for root [default=NULL]:"
		read ans
		PASS=$ans
	fi
	[ -z "$PASS" ] || PASS="-p$PASS"

	U=cxps
	P=cxps

	echo "Dropping $DB ..."
	mysqladmin -sf $USER $PASS drop $DB

	echo "Creating $DB ..."
	mysqladmin -sf $USER $PASS create $DB

	echo "Creating User $U ..."
	echo "CREATE USER '"$U"' IDENTIFIED BY '"$P"';" | mysql $USER $PASS
	echo "Granting privileges to local host ..."
	echo "GRANT ALL ON $DB.* to '"$U"'@'localhost' IDENTIFIED BY '"$P"';" | mysql $USER $PASS
	echo "Granting privileges to other hosts ..."
	echo "GRANT ALL ON $DB.* to '"$U"'@'%' IDENTIFIED BY '"$P"';" | mysql $USER $PASS
}

# Create DB for cas-server
casDb(){
	printf "Setting up DB for cas-server..."
	( cd ${CXPS_WORKSPACE}/cas-server ; createDb "CASDB" )
	[ $? -eq 0 ] && echo "Done!" || echo "Failed to create DB!"
}

# Create DB for Rice
riceDb(){
	printf "Setting up DB for Rice deployment..."
	( cd ${CXPS_WORKSPACE}/Rice/Deployment ; createDb "SEER_DB_2_0" )
	[ $? -eq 0 ] && echo "Done!" || echo "Failed to create DB!"
}

casDb 
riceDb
