
# ===========================================================================
# This file holds the global variable names for Unix utilities
# ===========================================================================
 
if [ -z "$__PORT_SH_EXECUTED" ] ; then

	SED=sed
	GREP=grep
	AWK=awk
	TAR=tar
	os=`uname -s`

	case "$os" in 
		"Linux")	p="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"
					;;
		"SunOS")	p="/opt/csw/bin:/usr/xpg4/bin:/usr/bin:/bin"
					SED=gsed
					AWK=gawk
					TAR=gtar
					GREP=ggrep
					DIFF=gdiff
					;;
		"Darwin")	p="/usr/bin:/bin:/usr/sbin:/sbin:/usr/X11/bin"
					;;
		"*")		p="/bin:/usr/bin:/usr/local/bin:/sbin:/usr/sbin:"
					;;
	esac

	export __PORT_SH_EXECUTED=executed

	PATH=$p:$PATH

	export SED GREP AWK TAR PATH
fi
