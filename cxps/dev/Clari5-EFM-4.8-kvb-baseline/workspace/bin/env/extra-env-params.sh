#!/usr/bin/env bash

#---------------------------------------------------------------------
# Adding the environment for [cdn-meta.conf]
#---------------------------------------------------------------------
export C3P0_HELPERS='10'
export C3P0_MAX_POOL='5'
export C3P0_MAX_STATEMENTS='0'
export C3P0_MIN_POOL='0'
export C3P0_TIMEOUT='0'
export JVM_MEM='2048'
# Value for [CC_PMSEED] already set
# Value for [CC_PMTYPE] already set
# Value for [DB_IP] already set
# Value for [DB_PORT] already set
# Value for [DB_SID] already set
# Value for [DB_TYPE] already set
# Value for [DB_USER] already set

#---------------------------------------------------------------------
# Adding the environment for [clari5-cc-meta.conf]
#---------------------------------------------------------------------
export C3P0_HELPERS='10'
export C3P0_MAX_POOL='5'
export C3P0_MAX_STATEMENTS='0'
export C3P0_MIN_POOL='0'
export C3P0_TIMEOUT='0'
export CC_REST_DEBUG='nodebug'
# Value for [JAVA_OPTS] already set
# Value for [SECURITY_IP] already set
# Value for [SECURITY_PMSEED] already set
# Value for [SECURITY_PMTYPE] already set
# Value for [SECURITY_PORT] already set
# Value for [SECURITY_SID] already set
# Value for [SECURITY_TYPE] already set
# Value for [SECURITY_USER] already set
# Value for [CC_PMSEED] already set
# Value for [CC_PMTYPE] already set
# Value for [DB_IP] already set
# Value for [DB_PORT] already set
# Value for [DB_SID] already set
# Value for [DB_TYPE] already set
# Value for [DB_USER] already set
export LOGIN_DN='anurag.customerxps.com'
# Value for [LOG_Q_LOCATION] already set
