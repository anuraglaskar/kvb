#!/usr/bin/env python

# ===========================================================================
# This utility converts all spec from camel case to 
# hyphenated words.
# Examples:
# AdityaNarainLal => aditya-narain-lal
# PICode => -p-i-code => -pi-code => pi-code
# codeBIC => code-b-i-c => code-bic
# ===========================================================================

import os, sys

def process(file):
	try :
		f = open(file,'r')
		lines = f.readlines()
		f.close()
	except :
		if f != None : f.close()

	output = []
	for line in lines :
		out = []
		for ch in line[:] :
			if ch.isalpha() and ch.isupper() :
				out.append("-" + ch.lower())
			else :
				out.append(ch)
		# For all continuous '-' join them
		i=0
		k=0
		fout = []
		while i < len(out) :
			fout.append(out[i])
			if out[i][0] == '-' :
				for j in range(i+1,len(out)) :
					if out[j][0] != '-' :
						break
					fout[k] += out[j][1]
					# print '@@@: %s' % fout
				i=j
			else :
				i+=1
			k+=1

		for i in range(1, len(fout)):
			if fout[i][0] == '-' and not fout[i-1][-1].isalnum() :
				fout[i] = fout[i][1:]

		output.append("".join(fout))

	try :
		f = open(file,'w')
		f.write("".join(output))
		f.close()
	except :
		if f != None : f.close()

if __name__ == '__main__' :
	for file in sys.argv[1:] :
		process(file)
