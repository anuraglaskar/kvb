cxps.events.event.nft-dblogin{
table-name : EVENT_NFT_DBLOGIN
event-mnemonic : LN
        workspaces : {
        ACCOUNT : "account-id"
        CUSTOMER : "cust-id"
        NONCUSTOMER : "addr-network"

}
event-attributes : {

channel:{ type:"string:20"}
user-id:{ type:"string:20"}
corporate-id:{ type:"string:20"}
cust-id:{ type:"string:20", fr : true}
account-id:{ type:"string:20", fr : true}
mobile-no:{ type:"string:20"}
device-id:{ type:"string:20"}
addr-network:{ type:"string:20", fr : true}
ip-country:{ type:"string:20"}
country-code:{ type:"string:20", derivation :"""cxps.events.CustomDerivator.getCountryCode(this)"""}
succ-fail-flg:{ type:"string:2"}
error-code:{ type:"string:20"}
error-desc:{ type:"string:50"}
user-type:{ type:"string:20"}
device-type:{ type:"string:20"}
logints:{ type:timestamp}
last-login-ip:{ type:"string:20"}
last-logints:{ type:timestamp}
mfa-type:{ type:"string:20"}
email-id:{ type:"string:20"}
is-pool-acct:{type:"string:20" , derivation :"""cxps.events.CustomDerivator.getIsPoolAcct(this)"""}
cust-name:{ type:"string:50" , derivation :"""cxps.events.CustomDerivator.getCustName(this)"""}
cust-mobile:{ type:"string:20" , derivation :"""cxps.events.CustomDerivator.getCustMobile(this)"""}
}
}
