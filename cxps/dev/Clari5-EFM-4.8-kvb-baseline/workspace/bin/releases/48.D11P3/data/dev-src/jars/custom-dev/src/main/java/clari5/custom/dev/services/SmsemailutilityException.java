package clari5.custom.dev.services;

public class SmsemailutilityException extends Exception {
    public SmsemailutilityException(String errorMessage) {
        super(errorMessage);
    }
}