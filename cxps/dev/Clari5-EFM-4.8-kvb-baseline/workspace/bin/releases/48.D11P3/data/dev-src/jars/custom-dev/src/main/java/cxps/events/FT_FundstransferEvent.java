// -- ASSISTED CODE --
package cxps.events;

import java.util.Set;
import java.util.HashSet;
import java.util.Date;
import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonIgnore;

import clari5.platform.aef.AEF;
import clari5.platform.aef.annotations.Field;
import clari5.platform.aef.annotations.Table;
import clari5.platform.aef.features.ITable;
import clari5.platform.util.CxJson;
import clari5.adb.IArchivable;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.logger.CXLog;
import clari5.platform.logger.ICXLog;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;

import com.mysql.jdbc.StringUtils;
import cxps.apex.noesis.WorkspaceInfo;
import cxps.noesis.core.Event;
import cxps.noesis.core.EventHelper;
import org.apache.tools.ant.taskdefs.Echo;


@Table(Name="EVENT_FT_FUNDSTRANSFER", Schema="rice")
public class FT_FundstransferEvent extends Event implements IArchivable {

    @Field(size = 100, key=true) public String eventId;
    @Field                       public Timestamp eventDate;
    @Field                       public Boolean isPostTransaction;
       @Field(size=20) public String payeeCode;
       @Field(size=20) public String channel;
       @Field(size=20) public String payerAcctId;
       @Field(size=50) public String payerName;
       @Field(size=20) public String aadharNo;
       @Field(size=20) public String deviceId;
       @Field(size=20) public String payeeAcctId;
       @Field(size=20) public String payerIfscCode;
       @Field(size=20) public String agentCode;
       @Field(size=20) public String tranSubCategory;
       @Field(size=20) public String accountType;
       @Field(size=20) public String tranId;
       @Field(size=2) public String isPoolAcct;
       @Field(size=20) public Double tranAmt;
       @Field(size=20) public String accountId;
       @Field(size=50) public String custName;
       @Field(size=50) public String payeeName;
       @Field(size=20) public String branchId;
       @Field(size=20) public String senderVpa;
       @Field(size=20) public String tranCrncyCode;
       @Field(size=50) public String tranRmks;
       @Field(size=20) public String terminalId;
       @Field(size=50) public String acctName;
       @Field(size=20) public String custCardId;
       @Field(size=20) public String payeeIfscCode;
       @Field(size=20) public Double avlBal;
       @Field(size=20) public String payeeId;
       @Field(size=20) public String payerMobNo;
       @Field(size=2) public String isRecPoolAcct;
       @Field(size=20) public String corporateId;
       @Field(size=20) public String countryCode;
       @Field(size=20) public String cityCode;
       @Field(size=20) public String succFailFlg;
       @Field(size=20) public String payeeNickName;
       @Field public java.sql.Timestamp lastCustInducedDt;
       @Field(size=20) public String custMobile;
       @Field(size=20) public String tranCategory;
       @Field(size=20) public String payerNickName;
       @Field(size=20) public String payerCode;
       @Field(size=20) public String receiverVpa;
       @Field(size=2) public String kvbCustomer;
       @Field(size=20) public String userId;
       @Field(size=20) public String custId;
       @Field(size=20) public String authType;
       @Field(size=20) public String payerId;
       @Field(size=20) public String networkType;
       @Field(size=20) public String ipAddress;
       @Field(size=20) public String tranType;
       @Field(size=20) public String mobileNo;
       @Field(size=20) public String mccId;
       @Field(size=20) public String payeeMobNo;
       @Field(size=20) public String partTranType;


    @JsonIgnore
    public ITable<FT_FundstransferEvent> t = AEF.getITable(this);

	public FT_FundstransferEvent(){}

    public FT_FundstransferEvent(CxConnection con, Date from, Date to){
      this.t.setTblName(makeArchivable(this.t.getTableName(), con, from, to));
    }

    /**
    * This method is used to populate the event object using the data
    * coming in message body of event json from external system.
    */
    @Override
    public void from(CxJson json) {
        super.from(json);
        setEventType("FT");
        setEventSubType("Fundstransfer");
        setEventName(getEventType() + "_" + getEventSubType());
        setEventId((!isPostTransaction() ? "RDA_" : "")+ json.getString("event_id"));

        if(getEventTS() != null)
            setEventDate(new Timestamp(getEventTS().getTime()));
        else
            setEventDate(new Timestamp(System.currentTimeMillis()));

            setPayeeCode(json.getString("payee_code"));
            setChannel(json.getString("channel"));
            setPayerAcctId(json.getString("payer_acct_id"));
            setPayerName(json.getString("payer_name"));
            setAadharNo(json.getString("aadhar_no"));
            setDeviceId(json.getString("device_id"));
            setPayeeAcctId(json.getString("payee_acct_id"));
            setPayerIfscCode(json.getString("payer_ifsc_code"));
            setAgentCode(json.getString("agent_code"));
            setTranSubCategory(json.getString("tran_sub_category"));
            setAccountType(json.getString("account_type"));
            setTranId(json.getString("tran_id"));
            setTranAmt(EventHelper.toDouble(json.getString("tran_amt")));
            setAccountId(json.getString("account_id"));
            setPayeeName(json.getString("payee_name"));
            setBranchId(json.getString("branch_id"));
            setSenderVpa(json.getString("sender_vpa"));
            setTranCrncyCode(json.getString("tran_crncy_code"));
            setTranRmks(json.getString("tran_rmks"));
            setTerminalId(json.getString("terminal_id"));
            setAcctName(json.getString("acct_name"));
            setCustCardId(json.getString("cust_card_id"));
            setPayeeIfscCode(json.getString("payee_ifsc_code"));
            setPayeeId(json.getString("payee_id"));
            setPayerMobNo(json.getString("payer_mob_no"));
            setCorporateId(json.getString("corporate_id"));
            setCityCode(json.getString("city_code"));
            setSuccFailFlg(json.getString("succ_fail_flg"));
            setPayeeNickName(json.getString("payee_nick_name"));
            setTranCategory(json.getString("tran_category"));
            setPayerNickName(json.getString("payer_nick_name"));
            setPayerCode(json.getString("payer_code"));
            setReceiverVpa(json.getString("receiver_vpa"));
            setKvbCustomer(json.getString("kvb_customer"));
            setUserId(json.getString("user_id"));
            setCustId(json.getString("cust_id"));
            setAuthType(json.getString("auth_type"));
            setPayerId(json.getString("payer_id"));
            setNetworkType(json.getString("network_type"));
            setIpAddress(json.getString("ip_address"));
            setTranType(json.getString("tran_type"));
            setMobileNo(json.getString("mobile_no"));
            setMccId(json.getString("mcc_id"));
            setPayeeMobNo(json.getString("payee_mob_no"));
            setPartTranType(json.getString("part_tran_type"));

        setDerivedValues();

    }


    private void setDerivedValues() {
        setIsPoolAcct(cxps.events.CustomDerivator.getIsPoolAcct(this));setCustName(cxps.events.CustomDerivator.getCustName(this));setAvlBal(cxps.events.CustomDerivator.getAvlBal(this));setIsRecPoolAcct(cxps.events.CustomDerivator.getIsRecPoolAcct(this));setCountryCode(cxps.events.CustomDerivator.getCountryCode(this));setLastCustInducedDt(cxps.events.CustomDerivator.getLastIndDate(this));setCustMobile(cxps.events.CustomDerivator.getCustMobile(this));
    }


    /* Getters */
    @Override
    public String getMnemonic() { return "FUND"; }

    public String getEventId() { return this.eventId;}
    public Timestamp getEventDate() { return this.eventDate; }
    public String getPayeeCode(){ return payeeCode; }

    public String getChannel(){ return channel; }

    public String getPayerAcctId(){ return payerAcctId; }

    public String getPayerName(){ return payerName; }

    public String getAadharNo(){ return aadharNo; }

    public String getDeviceId(){ return deviceId; }

    public String getPayeeAcctId(){ return payeeAcctId; }

    public String getPayerIfscCode(){ return payerIfscCode; }

    public String getAgentCode(){ return agentCode; }

    public String getTranSubCategory(){ return tranSubCategory; }

    public String getAccountType(){ return accountType; }

    public String getTranId(){ return tranId; }

    public Double getTranAmt(){ return tranAmt; }

    public String getAccountId(){ return accountId; }

    public String getPayeeName(){ return payeeName; }

    public String getBranchId(){ return branchId; }

    public String getSenderVpa(){ return senderVpa; }

    public String getTranCrncyCode(){ return tranCrncyCode; }

    public String getTranRmks(){ return tranRmks; }

    public String getTerminalId(){ return terminalId; }

    public String getAcctName(){ return acctName; }

    public String getCustCardId(){ return custCardId; }

    public String getPayeeIfscCode(){ return payeeIfscCode; }

    public String getPayeeId(){ return payeeId; }

    public String getPayerMobNo(){ return payerMobNo; }

    public String getCorporateId(){ return corporateId; }

    public String getCityCode(){ return cityCode; }

    public String getSuccFailFlg(){ return succFailFlg; }

    public String getPayeeNickName(){ return payeeNickName; }

    public String getTranCategory(){ return tranCategory; }

    public String getPayerNickName(){ return payerNickName; }

    public String getPayerCode(){ return payerCode; }

    public String getReceiverVpa(){ return receiverVpa; }

    public String getKvbCustomer(){ return kvbCustomer; }

    public String getUserId(){ return userId; }

    public String getCustId(){ return custId; }

    public String getAuthType(){ return authType; }

    public String getPayerId(){ return payerId; }

    public String getNetworkType(){ return networkType; }

    public String getIpAddress(){ return ipAddress; }

    public String getTranType(){ return tranType; }

    public String getMobileNo(){ return mobileNo; }

    public String getMccId(){ return mccId; }

    public String getPayeeMobNo(){ return payeeMobNo; }

    public String getPartTranType(){ return partTranType; }
    public String getIsPoolAcct(){ return isPoolAcct; }

    public String getCustName(){ return custName; }

    public Double getAvlBal(){ return avlBal; }

    public String getIsRecPoolAcct(){ return isRecPoolAcct; }

    public String getCountryCode(){ return countryCode; }

    public java.sql.Timestamp getLastCustInducedDt(){ return lastCustInducedDt; }

    public String getCustMobile(){ return custMobile; }

    /* Setters */
    public void setEventId(String val) { this.eventId = val; }
    public void setEventDate(Timestamp val) { this.eventDate = val; }
    public void setPayeeCode(String val){ this.payeeCode = val; }
    public void setChannel(String val){ this.channel = val; }
    public void setPayerAcctId(String val){ this.payerAcctId = val; }
    public void setPayerName(String val){ this.payerName = val; }
    public void setAadharNo(String val){ this.aadharNo = val; }
    public void setDeviceId(String val){ this.deviceId = val; }
    public void setPayeeAcctId(String val){ this.payeeAcctId = val; }
    public void setPayerIfscCode(String val){ this.payerIfscCode = val; }
    public void setAgentCode(String val){ this.agentCode = val; }
    public void setTranSubCategory(String val){ this.tranSubCategory = val; }
    public void setAccountType(String val){ this.accountType = val; }
    public void setTranId(String val){ this.tranId = val; }
    public void setTranAmt(Double val){ this.tranAmt = val; }
    public void setAccountId(String val){ this.accountId = val; }
    public void setPayeeName(String val){ this.payeeName = val; }
    public void setBranchId(String val){ this.branchId = val; }
    public void setSenderVpa(String val){ this.senderVpa = val; }
    public void setTranCrncyCode(String val){ this.tranCrncyCode = val; }
    public void setTranRmks(String val){ this.tranRmks = val; }
    public void setTerminalId(String val){ this.terminalId = val; }
    public void setAcctName(String val){ this.acctName = val; }
    public void setCustCardId(String val){ this.custCardId = val; }
    public void setPayeeIfscCode(String val){ this.payeeIfscCode = val; }
    public void setPayeeId(String val){ this.payeeId = val; }
    public void setPayerMobNo(String val){ this.payerMobNo = val; }
    public void setCorporateId(String val){ this.corporateId = val; }
    public void setCityCode(String val){ this.cityCode = val; }
    public void setSuccFailFlg(String val){ this.succFailFlg = val; }
    public void setPayeeNickName(String val){ this.payeeNickName = val; }
    public void setTranCategory(String val){ this.tranCategory = val; }
    public void setPayerNickName(String val){ this.payerNickName = val; }
    public void setPayerCode(String val){ this.payerCode = val; }
    public void setReceiverVpa(String val){ this.receiverVpa = val; }
    public void setKvbCustomer(String val){ this.kvbCustomer = val; }
    public void setUserId(String val){ this.userId = val; }
    public void setCustId(String val){ this.custId = val; }
    public void setAuthType(String val){ this.authType = val; }
    public void setPayerId(String val){ this.payerId = val; }
    public void setNetworkType(String val){ this.networkType = val; }
    public void setIpAddress(String val){ this.ipAddress = val; }
    public void setTranType(String val){ this.tranType = val; }
    public void setMobileNo(String val){ this.mobileNo = val; }
    public void setMccId(String val){ this.mccId = val; }
    public void setPayeeMobNo(String val){ this.payeeMobNo = val; }
    public void setPartTranType(String val){ this.partTranType = val; }
    public void setIsPoolAcct(String val){ this.isPoolAcct = val; }
    public void setCustName(String val){ this.custName = val; }
    public void setAvlBal(Double val){ this.avlBal = val; }
    public void setIsRecPoolAcct(String val){ this.isRecPoolAcct = val; }
    public void setCountryCode(String val){ this.countryCode = val; }
    public void setLastCustInducedDt(java.sql.Timestamp val){ this.lastCustInducedDt = val; }
    public void setCustMobile(String val){ this.custMobile = val; }

    /* Custom Getters*/


    /**
    * This method is used to return a set of WorkspaceInfo which contains the
    * information about the workspaces and it's corresponding entity which can be
    * derived for an event.
    */
    @Override
    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
        ICXLog cxLog = CXLog.fenter("derive.FT_FundstransferEvent");
        Set<WorkspaceInfo> wsInfoSet = new HashSet<>();

        CxKeyHelper h = Hfdb.getCxKeyHelper();

        if (!this.accountId.equals("XXXXXX") && !this.accountId.equals("NA")) {
            if (!StringUtils.isNullOrEmpty(accountId)) {
                String accountKey = h.getCxKeyGivenHostKey(WorkspaceName.ACCOUNT, getHostId(), this.accountId);
                wsInfoSet.add(new WorkspaceInfo("Account", accountKey));
            }
        }
        String nonKey = this.payeeAcctId + this.payerIfscCode;
        if (!nonKey.equals("XXXXXX") && !nonKey.equals("NA")) {
            if (!StringUtils.isNullOrEmpty(payeeAcctId + payeeIfscCode)) {
                String noncustomerKey = h.getCxKeyGivenHostKey(WorkspaceName.NONCUSTOMER, getHostId(), this.payeeAcctId + payeeIfscCode);
                wsInfoSet.add(new WorkspaceInfo("Noncustomer", noncustomerKey));
            }
        }
        if (!this.payerMobNo.equals("XXXXXX") && !this.payerMobNo.equals("NA")) {
            if (!StringUtils.isNullOrEmpty(payerMobNo)) {
                String noncustomerKey1 = h.getCxKeyGivenHostKey(WorkspaceName.NONCUSTOMER, getHostId(), this.payerMobNo);
                wsInfoSet.add(new WorkspaceInfo("Noncustomer", noncustomerKey1));
            }
        }
        if (!this.custId.equals("XXXXXX") && !this.custId.equals("NA")) {
            if (!StringUtils.isNullOrEmpty(custId)) {
                String customerKey = h.getCxKeyGivenHostKey(WorkspaceName.CUSTOMER, getHostId(), this.custId);
                wsInfoSet.add(new WorkspaceInfo("Customer", customerKey));
            }
        }

        cxLog.fexit();
        return wsInfoSet;
    }


    /**
    * This method is used to return a json to be shown as evidence on CMS (48.F+ release).
    */
    @JsonIgnore
    @Override
    public CxJson getEvidenceAsJson() {
        CxJson json = new CxJson();
        json.put("host_id", getHostId());
        return json;
    }

    /**
    * This method is used to return a json to be used in Fraud Resolution.
    * The fields present in this json are the fields which were marked with 'fr:true' in
    * the event spec along with some mandatory fields like : event_id, event_name, event_type, event_sub_type, sys_time.
    */
    @JsonIgnore
    @Override
    public CxJson getFRAsJson() {
        CxJson json = new CxJson();
        json.put("event_id", getEventId());
        json.put("event_name", "FT_Fundstransfer");
        json.put("event_type", "FT");
        json.put("event_sub_type", "Fundstransfer");
        if(getEventTS() != null) json.put("sys_time", getEventTS().getTime());
        if (!StringUtils.isNullOrEmpty(getPayeeAcctId())) {
            json.put("payee_acct_id", getPayeeAcctId());
        }
        else
            json.put("payee_acct_id", "XXXXXX");

        if (!StringUtils.isNullOrEmpty(getAccountId())) {
            json.put("account_id", getAccountId());
        }
        else
            json.put("account_id", "XXXXXX");

        if (!StringUtils.isNullOrEmpty(getPayeeIfscCode())) {
            json.put("payee_ifsc_code", getPayeeIfscCode());
        }
        else
            json.put("payee_ifsc_code", "XXXXXX");

        if (!StringUtils.isNullOrEmpty(getPayerMobNo())) {
            json.put("payer_mob_no", getPayerMobNo());
        }
        else
            json.put("payer_mob_no", "XXXXXX");

        if (!StringUtils.isNullOrEmpty(getCustId())) {
            json.put("cust_id", getCustId());
        }
        else
            json.put("cust_id", "XXXXXX");
        return json;
    }


    /**
    * This method is supposed to return a proper user understandable
    * message to be shown on the UI for end user.
    * For ex: for FT_AccountTxnEvent the message can be -> User has debited Rs 10000/- from
    * his account on 10th Feb, 2017 at 01:00 pm
    */
    @JsonIgnore
    @Override
    public String getEvidenceMessage(String factname) {
        String defaultMessage = ""; //TODO to be implemented by the developer.
        String message = EventHelper.evidenceMessage(this, factname);
        if(message != null && !"".equals(message)) return message;
        return defaultMessage;
    }
}
