cxps.noesis.glossary.entity.dda-addtls {
       db-name = DDA_ADDTLS
       generate = false
       db_column_quoted = true

       tablespace = CXPS_USERS
                       attributes:[
                               { name: ACCOUNT_NO, type:"string:20", key=true}
                               { name: AVLBAL, type:"number:20,3"}
				{ name: LAST_CUST_INDUCED_DT, type:timestamp}
                               ]
                       }
