cxps.events.event.nft-ibpwdcng{
table-name : EVENT_NFT_IBPWDCNG
event-mnemonic : IPC
        workspaces : {
        CUSTOMER : "cust-id"
	ACCOUNT : "account-id"

}
event-attributes : {
succ-fail-flg:{type:"string:2"}
error-code:{type:"string:20"}
corporate-id:{type:"string:20"}
addr-network:{type:"string:20"}
ip-country:{type:"string:20"}
ip-city:{type:"string:20"}
error-desc:{type:"string:50"}
user-id:{type:"string:20"}
device-type:{type:"string:20"}
device-id:{type:"string:20"}
cust-id:{type:"string:20", fr : true}
type:{type:"string:20"}
cng-mode:{type:"string:20"}
reset-using:{type:"string:20"}
country:{type:"string:20"}
channel:{type:"string:20"}
mobile-no:{type:"string:20"}
account-id:{type:"string:20", fr : true}
}
}
