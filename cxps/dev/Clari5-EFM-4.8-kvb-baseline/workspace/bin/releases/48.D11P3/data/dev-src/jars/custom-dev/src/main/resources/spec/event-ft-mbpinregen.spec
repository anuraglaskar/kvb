cxps.events.event.nft-mbpinregen{
table-name : EVENT_NFT_MBPINREGEN
event-mnemonic : MPR
        workspaces : {
        CUSTOMER : "cust-id"
	ACCOUNT : "account-id"

}
event-attributes : {

channel:{type:"string:20"}
mobile-no:{ type:"string:20"}	
user-id	:{ type:"string:20"}
account-id:{ type:"string:20", fr : true}
cust-id:{ type:"string:20", fr : true}
device-id:{ type:"string:20"}	
succ-fail-flg:{ type:"string:20"}
country	:{ type:"string:20"}
}
}
