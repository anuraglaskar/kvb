cxps.noesis.glossary.entity.high-risk-country {
       db-name = HIGH_RISK_COUNTRY
       generate = false
       db_column_quoted = true

       tablespace = CXPS_USERS
                       attributes:[
                               { name: COUNTRY_CODE, type="string:10", key=true}
                               { name: COUNTRY_NAME, type="string:10"}
                               { name: CREATED_ON ,type ="date"}

                               ]
                       }
