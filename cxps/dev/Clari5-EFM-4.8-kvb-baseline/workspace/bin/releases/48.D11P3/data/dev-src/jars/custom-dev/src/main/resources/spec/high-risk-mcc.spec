cxps.noesis.glossary.entity.high-risk-mcc {
       db-name = HIGH_RISK_MCC
       generate = false
       db_column_quoted = true

       tablespace = CXPS_USERS
                       attributes:[
                               { name: MCC_CODE, type="string:10", key=true}
                               { name: CREATED_ON ,type ="date"}

                               ]
                       }
