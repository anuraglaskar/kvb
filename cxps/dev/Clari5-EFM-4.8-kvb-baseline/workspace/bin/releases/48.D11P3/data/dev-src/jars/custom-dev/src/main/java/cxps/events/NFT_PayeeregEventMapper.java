// -- ASSISTED CODE --
package cxps.events;

import clari5.platform.dbcon.CxConnection;
import clari5.platform.dbcon.DbTypeEnum;
import clari5.trace.ConnectionWrapper;
import clari5.trace.ConnectionWrapperException;
import clari5.trace.mappers.EventMapper;
import cxps.apex.shared.IWSEvent;
import cxps.noesis.core.EventHelper;

import com.google.protobuf.ServiceException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;


import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

public class NFT_PayeeregEventMapper extends EventMapper<NFT_PayeeregEvent> {

public NFT_PayeeregEventMapper () {}

@Override
public void save(ConnectionWrapper connectionWrapper, List<NFT_PayeeregEvent> list) throws ConnectionWrapperException {
  switch (connectionWrapper.getType()){
    case HBASE:
        saveInHbase(connectionWrapper.getHbaseConnection(),list);
        break;
    case RDBMS:
        saveInRDBMS(connectionWrapper.getCxConnection(),list);
        break;
    }
}


@Override
public List<IWSEvent> getAllEvents(ConnectionWrapper connectionWrapper, Set<String> set) throws ConnectionWrapperException {
   List<IWSEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      allevents = getAllEventsFromHbase(connectionWrapper.getHbaseConnection(),set);
      return allevents;
    case RDBMS:
      allevents = getAllEventsFromRDBMS(connectionWrapper.getCxConnection(),set);
      return allevents;
   }
   return null;
}

@Override
public List<NFT_PayeeregEvent> getAllEventsFromArchive(ConnectionWrapper connectionWrapper, Set<String> set, Date from, Date to) throws ConnectionWrapperException {
   List<NFT_PayeeregEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      return new ArrayList<>();
    case RDBMS:
      allevents = getArchivedEventsFromRDBMS(connectionWrapper.getCxConnection(),set, from, to);
      return allevents;
   }
   return new ArrayList<>();
}


public void saveInRDBMS(CxConnection con,  List<NFT_PayeeregEvent> eventList) throws ConnectionWrapperException {
 if(eventList == null) return;
 if(eventList.isEmpty()) return;
 if(!NFT_PayeeregEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

 try (PreparedStatement preparedStatement = con.prepareStatement(this.saveQuery(con.getDbType()))) {
        for (NFT_PayeeregEvent obj : eventList) {
            int i = 1;
            preparedStatement.setString(i++,obj.getEventId());
            preparedStatement.setTimestamp(i++,obj.getEventDate());
            preparedStatement.setString(i++, obj.isPostTransaction() ? "Y" : "N");
            preparedStatement.setString(i++, obj.getCountry());
            preparedStatement.setString(i++, obj.getPayeeAccountId());
            preparedStatement.setString(i++, obj.getPayeeType());
            preparedStatement.setString(i++, obj.getPayeeNickname());
            preparedStatement.setString(i++, obj.getPayeeAccounttype());
            preparedStatement.setString(i++, obj.getPayeeBranchName());
            preparedStatement.setString(i++, obj.getChannel());
            preparedStatement.setString(i++, obj.getErrorDesc());
            preparedStatement.setString(i++, obj.getSuccFailFlg());
            preparedStatement.setString(i++, obj.getDeviceId());
            preparedStatement.setString(i++, obj.getPayeeBankName());
            preparedStatement.setString(i++, obj.getPayeeAccountName());
            preparedStatement.setString(i++, obj.getAddrNetwork());
            preparedStatement.setString(i++, obj.getPayeeBranchId());
            preparedStatement.setString(i++, obj.getPayeeStatus());
            preparedStatement.setDouble(i++, obj.getPayeeLimit());
            preparedStatement.setString(i++, obj.getAccountId());
            preparedStatement.setString(i++, obj.getUserId());
            preparedStatement.setString(i++, obj.getPayeeName());
            preparedStatement.setString(i++, obj.getCustId());
            preparedStatement.setString(i++, obj.getErrorCode());
            preparedStatement.setString(i++, obj.getMfaType());
            preparedStatement.setString(i++, obj.getNetworkType());
            preparedStatement.setString(i++, obj.getPayeeIfscCode());
            preparedStatement.setString(i++, obj.getPayeeBankId());
            preparedStatement.setString(i++, obj.getPayeeId());
            preparedStatement.setString(i++, obj.getMobileNo());
            preparedStatement.setString(i++, obj.getVpId());

            preparedStatement.addBatch();
        }
        preparedStatement.executeBatch();
    }catch (Exception e) {
        System.out.println("(RDBMS) Exception in saving data in [EVENT_NFT_PAYEEREG]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    }
}

public void saveInHbase(Connection connection, List<NFT_PayeeregEvent> eventList) throws ConnectionWrapperException {
    if(eventList == null) return;
    if(eventList.isEmpty()) return;
    if(!NFT_PayeeregEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

    Table table = null;
    List <Put> putList = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_NFT_PAYEEREG"));
        putList = new ArrayList<>();

        for (NFT_PayeeregEvent obj : eventList){

            Put p = new Put(Bytes.toBytes(obj.getEventId()));
            Timestamp ts = obj.getEventDate();
            p = this.insert(p,"EVENT_ID",obj.getEventId());
            p = this.insert(p,"EVENT_DATE", ts == null ? null : String.valueOf(ts));
            p = this.insert(p,"IS_POST_TRANSACTION",obj.isPostTransaction() ? "Y" : "N");
            p = this.insert(p, "COUNTRY",  obj.getCountry());
            p = this.insert(p, "PAYEE_ACCOUNT_ID",  obj.getPayeeAccountId());
            p = this.insert(p, "PAYEE_TYPE",  obj.getPayeeType());
            p = this.insert(p, "PAYEE_NICKNAME",  obj.getPayeeNickname());
            p = this.insert(p, "PAYEE_ACCOUNTTYPE",  obj.getPayeeAccounttype());
            p = this.insert(p, "PAYEE_BRANCH_NAME",  obj.getPayeeBranchName());
            p = this.insert(p, "CHANNEL",  obj.getChannel());
            p = this.insert(p, "ERROR_DESC",  obj.getErrorDesc());
            p = this.insert(p, "SUCC_FAIL_FLG",  obj.getSuccFailFlg());
            p = this.insert(p, "DEVICE_ID",  obj.getDeviceId());
            p = this.insert(p, "PAYEE_BANK_NAME",  obj.getPayeeBankName());
            p = this.insert(p, "PAYEE_ACCOUNT_NAME",  obj.getPayeeAccountName());
            p = this.insert(p, "ADDR_NETWORK",  obj.getAddrNetwork());
            p = this.insert(p, "PAYEE_BRANCH_ID",  obj.getPayeeBranchId());
            p = this.insert(p, "PAYEE_STATUS",  obj.getPayeeStatus());
            p = this.insert(p, "PAYEE_LIMIT", String.valueOf(obj.getPayeeLimit()));
            p = this.insert(p, "ACCOUNT_ID",  obj.getAccountId());
            p = this.insert(p, "USER_ID",  obj.getUserId());
            p = this.insert(p, "PAYEE_NAME",  obj.getPayeeName());
            p = this.insert(p, "CUST_ID",  obj.getCustId());
            p = this.insert(p, "ERROR_CODE",  obj.getErrorCode());
            p = this.insert(p, "MFA_TYPE",  obj.getMfaType());
            p = this.insert(p, "NETWORK_TYPE",  obj.getNetworkType());
            p = this.insert(p, "PAYEE_IFSC_CODE",  obj.getPayeeIfscCode());
            p = this.insert(p, "PAYEE_BANK_ID",  obj.getPayeeBankId());
            p = this.insert(p, "PAYEE_ID",  obj.getPayeeId());
            p = this.insert(p, "MOBILE_NO",  obj.getMobileNo());
            p = this.insert(p, "VP_ID",  obj.getVpId());
            putList.add(p);
        }
        table.put(putList);

    } catch (IOException e) {
        Admin admin = null;
        try {
            //if table does not exists, create table and insert again.
            admin = connection.getAdmin();
            HTableDescriptor hTableDescriptor = new HTableDescriptor(TableName.valueOf("EVENT_NFT_PAYEEREG"));
            hTableDescriptor.addFamily(new HColumnDescriptor("EVENT"));
            admin.createTable(hTableDescriptor);
            table.put(putList);
        } catch (Exception e1) {
            System.out.println("(HBASE) Exception in saving data in [EVENT_NFT_PAYEEREG]: "+e.getMessage());
            throw new ConnectionWrapperException(e1.getMessage(), e1.getCause());
        } finally {
            try {
                if (admin != null)
                    admin.close();
                if (table != null)
                    table.close();
            } catch (IOException e1) {
                System.out.println("(HBASE) Exception for [EVENT_NFT_PAYEEREG]: "+e.getMessage());
                throw new ConnectionWrapperException(e.getMessage(), e.getCause());
            }
        }
    }

}


public List<IWSEvent> getAllEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds) throws ConnectionWrapperException {
 List<IWSEvent> events = new ArrayList<>();
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");
 try{
 cxConnection.query(this.fetchQuery(cxConnection.getDbType()) + " where event_id in ("+ sb.toString() + ")", rs -> {
 while (rs.next()) {
    NFT_PayeeregEvent obj = new NFT_PayeeregEvent();
    obj.setEventId(rs.getString("EVENT_ID"));
    obj.setEventDate(rs.getTimestamp("EVENT_DATE"));
    obj.setEventTS(obj.getEventDate());
    obj.setPostTransaction(rs.getString("IS_POST_TRANSACTION").equals("Y"));

    obj.setCountry(rs.getString("COUNTRY"));
    obj.setPayeeAccountId(rs.getString("PAYEE_ACCOUNT_ID"));
    obj.setPayeeType(rs.getString("PAYEE_TYPE"));
    obj.setPayeeNickname(rs.getString("PAYEE_NICKNAME"));
    obj.setPayeeAccounttype(rs.getString("PAYEE_ACCOUNTTYPE"));
    obj.setPayeeBranchName(rs.getString("PAYEE_BRANCH_NAME"));
    obj.setChannel(rs.getString("CHANNEL"));
    obj.setErrorDesc(rs.getString("ERROR_DESC"));
    obj.setSuccFailFlg(rs.getString("SUCC_FAIL_FLG"));
    obj.setDeviceId(rs.getString("DEVICE_ID"));
    obj.setPayeeBankName(rs.getString("PAYEE_BANK_NAME"));
    obj.setPayeeAccountName(rs.getString("PAYEE_ACCOUNT_NAME"));
    obj.setAddrNetwork(rs.getString("ADDR_NETWORK"));
    obj.setPayeeBranchId(rs.getString("PAYEE_BRANCH_ID"));
    obj.setPayeeStatus(rs.getString("PAYEE_STATUS"));
    obj.setPayeeLimit(rs.getDouble("PAYEE_LIMIT"));
    obj.setAccountId(rs.getString("ACCOUNT_ID"));
    obj.setUserId(rs.getString("USER_ID"));
    obj.setPayeeName(rs.getString("PAYEE_NAME"));
    obj.setCustId(rs.getString("CUST_ID"));
    obj.setErrorCode(rs.getString("ERROR_CODE"));
    obj.setMfaType(rs.getString("MFA_TYPE"));
    obj.setNetworkType(rs.getString("NETWORK_TYPE"));
    obj.setPayeeIfscCode(rs.getString("PAYEE_IFSC_CODE"));
    obj.setPayeeBankId(rs.getString("PAYEE_BANK_ID"));
    obj.setPayeeId(rs.getString("PAYEE_ID"));
    obj.setMobileNo(rs.getString("MOBILE_NO"));
    obj.setVpId(rs.getString("VP_ID"));

    events.add(obj);
   }});
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting data from [EVENT_NFT_PAYEEREG]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<NFT_PayeeregEvent> getArchivedEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds, Date from, Date to) throws ConnectionWrapperException {
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");

 List<NFT_PayeeregEvent> events;
 NFT_PayeeregEvent obj = new NFT_PayeeregEvent(cxConnection, from, to);
 try{
     events = obj.t.loadAllFilter(cxConnection," where event_id in ("+ sb.toString() + ")", null, null);
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting archived data from ["+obj.t.getTableName()+"]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<IWSEvent> getAllEventsFromHbase(Connection connection, Set<String> eventIds) throws ConnectionWrapperException {

    List<IWSEvent> events = new ArrayList<>();
    List<Get> getList = new ArrayList<>();
    Table table = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_NFT_PAYEEREG"));
        for (String id : eventIds){
            Get g = new Get(Bytes.toBytes(id));
            getList.add(g);
        }
        Result[] results = table.get(getList);
        for (Result rs : results){
            NFT_PayeeregEvent obj = new NFT_PayeeregEvent();
            obj.setEventId(getColumnValue(rs, "EVENT_ID"));
            String ts = getColumnValue(rs, "EVENT_DATE");
            obj.setEventDate((ts == null || ts.trim().equalsIgnoreCase("")) ? null : new Timestamp(Long.parseLong(ts.trim())));
            obj.setEventTS(obj.getEventDate());
            obj.setPostTransaction(getColumnValue(rs,"IS_POST_TRANSACTION").equals("Y"));

            obj.setCountry(getColumnValue(rs, "COUNTRY"));
            obj.setPayeeAccountId(getColumnValue(rs, "PAYEE_ACCOUNT_ID"));
            obj.setPayeeType(getColumnValue(rs, "PAYEE_TYPE"));
            obj.setPayeeNickname(getColumnValue(rs, "PAYEE_NICKNAME"));
            obj.setPayeeAccounttype(getColumnValue(rs, "PAYEE_ACCOUNTTYPE"));
            obj.setPayeeBranchName(getColumnValue(rs, "PAYEE_BRANCH_NAME"));
            obj.setChannel(getColumnValue(rs, "CHANNEL"));
            obj.setErrorDesc(getColumnValue(rs, "ERROR_DESC"));
            obj.setSuccFailFlg(getColumnValue(rs, "SUCC_FAIL_FLG"));
            obj.setDeviceId(getColumnValue(rs, "DEVICE_ID"));
            obj.setPayeeBankName(getColumnValue(rs, "PAYEE_BANK_NAME"));
            obj.setPayeeAccountName(getColumnValue(rs, "PAYEE_ACCOUNT_NAME"));
            obj.setAddrNetwork(getColumnValue(rs, "ADDR_NETWORK"));
            obj.setPayeeBranchId(getColumnValue(rs, "PAYEE_BRANCH_ID"));
            obj.setPayeeStatus(getColumnValue(rs, "PAYEE_STATUS"));
            obj.setPayeeLimit(EventHelper.toDouble(getColumnValue(rs, "PAYEE_LIMIT")));
            obj.setAccountId(getColumnValue(rs, "ACCOUNT_ID"));
            obj.setUserId(getColumnValue(rs, "USER_ID"));
            obj.setPayeeName(getColumnValue(rs, "PAYEE_NAME"));
            obj.setCustId(getColumnValue(rs, "CUST_ID"));
            obj.setErrorCode(getColumnValue(rs, "ERROR_CODE"));
            obj.setMfaType(getColumnValue(rs, "MFA_TYPE"));
            obj.setNetworkType(getColumnValue(rs, "NETWORK_TYPE"));
            obj.setPayeeIfscCode(getColumnValue(rs, "PAYEE_IFSC_CODE"));
            obj.setPayeeBankId(getColumnValue(rs, "PAYEE_BANK_ID"));
            obj.setPayeeId(getColumnValue(rs, "PAYEE_ID"));
            obj.setMobileNo(getColumnValue(rs, "MOBILE_NO"));
            obj.setVpId(getColumnValue(rs, "VP_ID"));

            events.add(obj);
        }
        return events;

    } catch (IOException e) {
        System.out.println("(HBASE) Exception in getting data from [EVENT_NFT_PAYEEREG]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    } finally {
        if (table != null) try {
            table.close();
        } catch (IOException e) {
            System.out.println("(HBASE) Exception in getting data from [EVENT_NFT_PAYEEREG]: "+e.getMessage());
            throw new ConnectionWrapperException(e.getMessage(), e.getCause());
        }
    }
}


private String fetchQuery(DbTypeEnum dbTypeEnum) {
 switch (dbTypeEnum) {
  case ORACLE:
      return "SELECT \"EVENT_ID\", \"EVENT_DATE\",\"IS_POST_TRANSACTION\", \"COUNTRY\",\"PAYEE_ACCOUNT_ID\",\"PAYEE_TYPE\",\"PAYEE_NICKNAME\",\"PAYEE_ACCOUNTTYPE\",\"PAYEE_BRANCH_NAME\",\"CHANNEL\",\"ERROR_DESC\",\"SUCC_FAIL_FLG\",\"DEVICE_ID\",\"PAYEE_BANK_NAME\",\"PAYEE_ACCOUNT_NAME\",\"ADDR_NETWORK\",\"PAYEE_BRANCH_ID\",\"PAYEE_STATUS\",\"PAYEE_LIMIT\",\"ACCOUNT_ID\",\"USER_ID\",\"PAYEE_NAME\",\"CUST_ID\",\"ERROR_CODE\",\"MFA_TYPE\",\"NETWORK_TYPE\",\"PAYEE_IFSC_CODE\",\"PAYEE_BANK_ID\",\"PAYEE_ID\",\"MOBILE_NO\",\"VP_ID\"" +
              " FROM EVENT_NFT_PAYEEREG";
  case SQLSERVER:
      return "SELECT [EVENT_ID], [EVENT_DATE],[IS_POST_TRANSACTION], [COUNTRY],[PAYEE_ACCOUNT_ID],[PAYEE_TYPE],[PAYEE_NICKNAME],[PAYEE_ACCOUNTTYPE],[PAYEE_BRANCH_NAME],[CHANNEL],[ERROR_DESC],[SUCC_FAIL_FLG],[DEVICE_ID],[PAYEE_BANK_NAME],[PAYEE_ACCOUNT_NAME],[ADDR_NETWORK],[PAYEE_BRANCH_ID],[PAYEE_STATUS],[PAYEE_LIMIT],[ACCOUNT_ID],[USER_ID],[PAYEE_NAME],[CUST_ID],[ERROR_CODE],[MFA_TYPE],[NETWORK_TYPE],[PAYEE_IFSC_CODE],[PAYEE_BANK_ID],[PAYEE_ID],[MOBILE_NO],[VP_ID]" +
              " FROM EVENT_NFT_PAYEEREG";
  case MYSQL:
      return "SELECT `EVENT_ID`, `EVENT_DATE`,`IS_POST_TRANSACTION`,`COUNTRY`,`PAYEE_ACCOUNT_ID`,`PAYEE_TYPE`,`PAYEE_NICKNAME`,`PAYEE_ACCOUNTTYPE`,`PAYEE_BRANCH_NAME`,`CHANNEL`,`ERROR_DESC`,`SUCC_FAIL_FLG`,`DEVICE_ID`,`PAYEE_BANK_NAME`,`PAYEE_ACCOUNT_NAME`,`ADDR_NETWORK`,`PAYEE_BRANCH_ID`,`PAYEE_STATUS`,`PAYEE_LIMIT`,`ACCOUNT_ID`,`USER_ID`,`PAYEE_NAME`,`CUST_ID`,`ERROR_CODE`,`MFA_TYPE`,`NETWORK_TYPE`,`PAYEE_IFSC_CODE`,`PAYEE_BANK_ID`,`PAYEE_ID`,`MOBILE_NO`,`VP_ID`" +
              " FROM EVENT_NFT_PAYEEREG";
 }
  return null;
}

private String saveQuery(DbTypeEnum dbType) {

 String insertQuery="INSERT INTO EVENT_NFT_PAYEEREG (";
 switch(dbType){
    case ORACLE:
        insertQuery=insertQuery+"\"EVENT_ID\",\"EVENT_DATE\",\"IS_POST_TRANSACTION\",\"COUNTRY\",\"PAYEE_ACCOUNT_ID\",\"PAYEE_TYPE\",\"PAYEE_NICKNAME\",\"PAYEE_ACCOUNTTYPE\",\"PAYEE_BRANCH_NAME\",\"CHANNEL\",\"ERROR_DESC\",\"SUCC_FAIL_FLG\",\"DEVICE_ID\",\"PAYEE_BANK_NAME\",\"PAYEE_ACCOUNT_NAME\",\"ADDR_NETWORK\",\"PAYEE_BRANCH_ID\",\"PAYEE_STATUS\",\"PAYEE_LIMIT\",\"ACCOUNT_ID\",\"USER_ID\",\"PAYEE_NAME\",\"CUST_ID\",\"ERROR_CODE\",\"MFA_TYPE\",\"NETWORK_TYPE\",\"PAYEE_IFSC_CODE\",\"PAYEE_BANK_ID\",\"PAYEE_ID\",\"MOBILE_NO\",\"VP_ID\") values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case SQLSERVER:
        insertQuery=insertQuery+"[EVENT_ID],[EVENT_DATE],[IS_POST_TRANSACTION],[COUNTRY],[PAYEE_ACCOUNT_ID],[PAYEE_TYPE],[PAYEE_NICKNAME],[PAYEE_ACCOUNTTYPE],[PAYEE_BRANCH_NAME],[CHANNEL],[ERROR_DESC],[SUCC_FAIL_FLG],[DEVICE_ID],[PAYEE_BANK_NAME],[PAYEE_ACCOUNT_NAME],[ADDR_NETWORK],[PAYEE_BRANCH_ID],[PAYEE_STATUS],[PAYEE_LIMIT],[ACCOUNT_ID],[USER_ID],[PAYEE_NAME],[CUST_ID],[ERROR_CODE],[MFA_TYPE],[NETWORK_TYPE],[PAYEE_IFSC_CODE],[PAYEE_BANK_ID],[PAYEE_ID],[MOBILE_NO],[VP_ID]) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case MYSQL:
        insertQuery=insertQuery+"`EVENT_ID`,`EVENT_DATE`, `IS_POST_TRANSACTION`,`COUNTRY`,`PAYEE_ACCOUNT_ID`,`PAYEE_TYPE`,`PAYEE_NICKNAME`,`PAYEE_ACCOUNTTYPE`,`PAYEE_BRANCH_NAME`,`CHANNEL`,`ERROR_DESC`,`SUCC_FAIL_FLG`,`DEVICE_ID`,`PAYEE_BANK_NAME`,`PAYEE_ACCOUNT_NAME`,`ADDR_NETWORK`,`PAYEE_BRANCH_ID`,`PAYEE_STATUS`,`PAYEE_LIMIT`,`ACCOUNT_ID`,`USER_ID`,`PAYEE_NAME`,`CUST_ID`,`ERROR_CODE`,`MFA_TYPE`,`NETWORK_TYPE`,`PAYEE_IFSC_CODE`,`PAYEE_BANK_ID`,`PAYEE_ID`,`MOBILE_NO`,`VP_ID`) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
 }
  return insertQuery;
}

private Put insert(Put put,String name, String value){
 byte[] cf = Bytes.toBytes("EVENT");
 byte[] nameBytes = Bytes.toBytes(name);
 byte[] valueBytes = value == null ? null : Bytes.toBytes(value);
 return put.addColumn(cf,nameBytes,valueBytes);
 }

 private String getColumnValue(Result rs , String cName){
     byte[] cf = Bytes.toBytes("EVENT");
     byte[] name = Bytes.toBytes(cName);
     byte [] value = rs.getValue(cf,name);
     return value == null ? "" : Bytes.toString(value);
 }
}

