// -- ASSISTED CODE --
package cxps.events;

import java.util.Set;
import java.util.HashSet;
import java.util.Date;
import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonIgnore;

import clari5.platform.aef.AEF;
import clari5.platform.aef.annotations.Field;
import clari5.platform.aef.annotations.Table;
import clari5.platform.aef.features.ITable;
import clari5.platform.util.CxJson;
import clari5.adb.IArchivable;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.logger.CXLog;
import clari5.platform.logger.ICXLog;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;

import com.mysql.jdbc.StringUtils;
import cxps.apex.noesis.WorkspaceInfo;
import cxps.noesis.core.Event;
import cxps.noesis.core.EventHelper;


@Table(Name="EVENT_NFT_MBPINREGEN", Schema="rice")
public class NFT_MbpinregenEvent extends Event implements IArchivable {

    @Field(size = 100, key=true) public String eventId;
    @Field                       public Timestamp eventDate;
    @Field                       public Boolean isPostTransaction;
       @Field(size=20) public String country;
       @Field(size=20) public String accountId;
       @Field(size=20) public String userId;
       @Field(size=20) public String channel;
       @Field(size=20) public String mobileNo;
       @Field(size=20) public String succFailFlg;
       @Field(size=20) public String custId;
       @Field(size=20) public String deviceId;


    @JsonIgnore
    public ITable<NFT_MbpinregenEvent> t = AEF.getITable(this);

	public NFT_MbpinregenEvent(){}

    public NFT_MbpinregenEvent(CxConnection con, Date from, Date to){
      this.t.setTblName(makeArchivable(this.t.getTableName(), con, from, to));
    }

    /**
    * This method is used to populate the event object using the data
    * coming in message body of event json from external system.
    */
    @Override
    public void from(CxJson json) {
        super.from(json);
        setEventType("NFT");
        setEventSubType("Mbpinregen");
        setEventName(getEventType() + "_" + getEventSubType());
        setEventId((!isPostTransaction() ? "RDA_" : "")+ json.getString("event_id"));

        if(getEventTS() != null)
            setEventDate(new Timestamp(getEventTS().getTime()));
        else
            setEventDate(new Timestamp(System.currentTimeMillis()));

            setCountry(json.getString("country"));
            setAccountId(json.getString("account_id"));
            setUserId(json.getString("user_id"));
            setChannel(json.getString("channel"));
            setMobileNo(json.getString("mobile_no"));
            setSuccFailFlg(json.getString("succ_fail_flg"));
            setCustId(json.getString("cust_id"));
            setDeviceId(json.getString("device_id"));

        setDerivedValues();

    }


    private void setDerivedValues() {
    }


    /* Getters */
    @Override
    public String getMnemonic() { return "MPR"; }

    public String getEventId() { return this.eventId;}
    public Timestamp getEventDate() { return this.eventDate; }
    public String getCountry(){ return country; }

    public String getAccountId(){ return accountId; }

    public String getUserId(){ return userId; }

    public String getChannel(){ return channel; }

    public String getMobileNo(){ return mobileNo; }

    public String getSuccFailFlg(){ return succFailFlg; }

    public String getCustId(){ return custId; }

    public String getDeviceId(){ return deviceId; }

    /* Setters */
    public void setEventId(String val) { this.eventId = val; }
    public void setEventDate(Timestamp val) { this.eventDate = val; }
    public void setCountry(String val){ this.country = val; }
    public void setAccountId(String val){ this.accountId = val; }
    public void setUserId(String val){ this.userId = val; }
    public void setChannel(String val){ this.channel = val; }
    public void setMobileNo(String val){ this.mobileNo = val; }
    public void setSuccFailFlg(String val){ this.succFailFlg = val; }
    public void setCustId(String val){ this.custId = val; }
    public void setDeviceId(String val){ this.deviceId = val; }

    /* Custom Getters*/


    /**
    * This method is used to return a set of WorkspaceInfo which contains the
    * information about the workspaces and it's corresponding entity which can be
    * derived for an event.
    */
    @Override
    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
        ICXLog cxLog = CXLog.fenter("derive.NFT_MbpinregenEvent");
        Set<WorkspaceInfo> wsInfoSet = new HashSet<>();

        CxKeyHelper h = Hfdb.getCxKeyHelper();

        if (!this.accountId.equals("XXXXXX") && !this.accountId.equals("NA")) {
            if (!StringUtils.isNullOrEmpty(accountId)) {
                String accountKey = h.getCxKeyGivenHostKey(WorkspaceName.ACCOUNT, getHostId(), this.accountId);
                wsInfoSet.add(new WorkspaceInfo("Account", accountKey));
            }
        }

        if (!this.custId.equals("XXXXXX") && !this.custId.equals("NA")) {
            if (!StringUtils.isNullOrEmpty(custId)) {
                String customerKey = h.getCxKeyGivenHostKey(WorkspaceName.CUSTOMER, getHostId(), this.custId);
                wsInfoSet.add(new WorkspaceInfo("Customer", customerKey));
            }
        }

        cxLog.fexit();
        return wsInfoSet;
    }


    /**
    * This method is used to return a json to be shown as evidence on CMS (48.F+ release).
    */
    @JsonIgnore
    @Override
    public CxJson getEvidenceAsJson() {
        CxJson json = new CxJson();
        json.put("host_id", getHostId());
        return json;
    }

    /**
    * This method is used to return a json to be used in Fraud Resolution.
    * The fields present in this json are the fields which were marked with 'fr:true' in
    * the event spec along with some mandatory fields like : event_id, event_name, event_type, event_sub_type, sys_time.
    */
    @JsonIgnore
    @Override
    public CxJson getFRAsJson() {
        CxJson json = new CxJson();
        json.put("event_id", getEventId());
        json.put("event_name", "NFT_Mbpinregen");
        json.put("event_type", "NFT");
        json.put("event_sub_type", "Mbpinregen");
        if(getEventTS() != null) json.put("sys_time", getEventTS().getTime());
        if (!StringUtils.isNullOrEmpty(getAccountId())) {
            json.put("account_id", getAccountId());
        }
        json.put("account_id", "XXXXXX");

        if (!StringUtils.isNullOrEmpty(getCustId())) {
            json.put("cust_id", getCustId());
        }
        else
            json.put("cust_id", "XXXXXX");
        return json;
    }


    /**
    * This method is supposed to return a proper user understandable
    * message to be shown on the UI for end user.
    * For ex: for FT_AccountTxnEvent the message can be -> User has debited Rs 10000/- from
    * his account on 10th Feb, 2017 at 01:00 pm
    */
    @JsonIgnore
    @Override
    public String getEvidenceMessage(String factname) {
        String defaultMessage = ""; //TODO to be implemented by the developer.
        String message = EventHelper.evidenceMessage(this, factname);
        if(message != null && !"".equals(message)) return message;
        return defaultMessage;
    }
}