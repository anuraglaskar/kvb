package clari5.custom.dev.services.sendSms;

import clari5.custom.dev.DbUtil.Dbfetch;
import clari5.custom.dev.services.SmsemailutilityException;
import clari5.platform.util.Hocon;
import clari5.platform.logger.CxpsLogger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class Sendsms extends HttpServlet {
	private static CxpsLogger logger = CxpsLogger.getLogger(Sendsms.class);
	private static String responseTag = "";
	private static Hocon hcon = null;
	static {
		hcon = new Hocon();
		hcon.loadFromContext("sms-details.conf");
		responseTag = hcon.getString("responsetag");
	}
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException {
		String issue_key = req.getParameter("issuekey");
		String callBack = req.getParameter("callback");
		String response = "";
		String custId = "";
		String acctIdFull = "";
		String acctId = "";
		String tranDate = "";
		String phoneNum = "";

		PrintWriter out=null;
		String resp=null;
		out = res.getWriter();
		res.setContentType("application/javascript");

		logger.info("Preparing SMS");
		Dbfetch db = new Dbfetch();
		List<String> tranDetails = db.getTranDetails(issue_key);
		if(tranDetails.size() != 0) {
			if((tranDetails.get(0) == null || tranDetails.get(0).isEmpty()) || (tranDetails.get(1) == null || tranDetails.get(1).isEmpty()) || (tranDetails.get(2) == null || tranDetails.get(2).isEmpty())) {
				try{
					throw new SmsemailutilityException("Insufficient Data. Customer ID or Account ID or Transaction Date or Phone Number missing");
				}catch (Exception e) {
					e.printStackTrace();
				}
			} else {
				custId = tranDetails.get(0);
				acctIdFull = tranDetails.get(1);
				if (acctIdFull.length() > 4) {
					acctId = acctIdFull.substring(acctIdFull.length() - 4);
				} else {
					acctId = acctIdFull;
				}
				tranDate = tranDetails.get(2);
				phoneNum = db.getCustPhoneNum(custId);
				String smsresponse = new Soapcon().sendSoapReqFT(phoneNum, tranDate, acctId, issue_key, responseTag);

			/**
			Extracting SMS response message
			 */
				//System.out.println("Response Before Processing : " + response);
				response = new Soapcon().stringParser(smsresponse, responseTag);
			}
			if(response.length() < 1) {
				try {
					throw new SmsemailutilityException("Did not get any response from SMS Server. Check the SMS configuration");
				} catch (SmsemailutilityException e) {
					e.printStackTrace();
				}
				response = "Unsuccessfull.Check logs";
			}
			//System.out.println(response);
			logger.info(response);
			db.updateSmsEmailDetailsTable("SMS","",phoneNum,custId,response);

			response = "({\"Response\":\""+response+"\"})";
			resp = callBack+response;
			out.print(resp);
			out.flush();
			return;
		}
		else {
			logger.error("Could not find transaction details against the issue key "+issue_key);
			db.updateSmsEmailDetailsTable("SMS", "", phoneNum, custId, response);

			response = "({\"Response\":\"Failed to send sms\"})";
			resp = callBack+response;
			out.print(resp);
			out.flush();
			try {
				throw new SmsemailutilityException("Insufficient Data");
			} catch (SmsemailutilityException e) {
				e.printStackTrace();
			}
			return;
		}
	}
}