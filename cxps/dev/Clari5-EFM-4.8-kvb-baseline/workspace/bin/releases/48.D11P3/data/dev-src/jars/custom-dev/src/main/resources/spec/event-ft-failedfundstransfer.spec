cxps.events.event.ft-failedfundstransfer{
table-name : EVENT_FT_FAILEDFUNDSTRANSFER
event-mnemonic : FFT
        workspaces : {
        ACCOUNT : "account-id"
        CUSTOMER : "cust-id"

}
event-attributes : {
channel:{ type:"string:20"}
aadhar-no:{ type:"string:20"}
account-id:{ type:"string:20", fr : true}
cust-id:{ type:"string:20", fr : true}
corporate-id:{ type:"string:20"}
mobile-no:{ type:"string:20"}
user-id:{ type:"string:20"}
acct-name:{ type:"string:50"}
tran-type:{ type:"string:20"}
network-type:{ type:"string:20"}
auth-type:{ type:"string:20"}
tran-id:{ type:"string:20"}
tran-amt:{ type:"number:20,3"}
tran-crncy-code:{ type:"string:20"}
tran-rmks:{ type:"string:50"}
ip-address:{ type:"string:20"}
cust-card-id:{ type:"string:20"}
terminal-id:{ type:"string:20"}
device-id:{ type:"string:20"}
branch-id:{ type:"string:20"}
country-code:{ type:"string:20"}
payee-id:{ type:"string:20"}
payee-acct-id:{ type:"string:20"}
payee-nick-name:{ type:"string:20"}
payeename:{ type:"string:50"}
payee-ifsc-code:{ type:"string:20"}
payee-mob-no:{ type:"string:20"}
payee-code:{ type:"string:20"}
payer-id:{ type:"string:20"}
payer-acct-id:{ type:"string:20"}
payer-nick-name:{ type:"string:20"}
payer-name:{ type:"string:50"}
payer-ifsc-code:{ type:"string:20"}
payer-mob-no:{ type:"string:20"}
payer-code:{ type:"string:20"}
sender-vpa:{ type:"string:20"}
mcc-id:{ type:"string:20"}
city-code:{ type:"string:20"}
agent-code:{ type:"string:20"}
account-type:{ type:"string:20"}
tran-category:{ type:"string:20"}
tran-sub-category:{ type:"string:20"}
part-tran-type:{ type:"string:20"}
receiver-vpa:{ type:"string:20"}
kvb-customer:{ type:"string:20"}
error-code:{ type:"string:20"}
fail-type:{type:"string:20"}
succ-fail-flg:{type:"string:20"}
}
}
