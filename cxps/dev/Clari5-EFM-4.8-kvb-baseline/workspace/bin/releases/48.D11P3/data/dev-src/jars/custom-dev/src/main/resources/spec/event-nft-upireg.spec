cxps.events.event.nft-upireg{
table-name : EVENT_NFT_UPIREG
event-mnemonic : RG
        workspaces : {
        ACCOUNT : "account-id"
        NONCUSTOMER : "mobile-no"
}
event-attributes : {

channel:{ type:"string:20"}
cust-id:{ type:"string:20", fr : true}
mobile-no:{ type:"string:20", fr : true}
account-id:{ type:"string:20", fr : true}
device-id:{ type:"string:20"}
addr-network:{ type:"string:20"}
country:{ type:"string:20"}
succ-fail-flg:{ type:"string:2"}
error-code:{ type:"string:20"}
error-desc:{ type:"string:50"}
vp-id:{ type:"string:20"}
kvb-customer:{ type:"string:2"}
reg-source:{ type:"string:2"}
}
}
