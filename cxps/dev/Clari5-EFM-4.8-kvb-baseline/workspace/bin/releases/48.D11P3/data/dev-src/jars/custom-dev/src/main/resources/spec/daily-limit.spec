cxps.noesis.glossary.entity.daily-limit {
       db-name = DAILY_LIMIT
       generate = false
       db_column_quoted = true

       tablespace = CXPS_USERS
                       attributes:[
                               { name: TRAN_TYPE, type="string:20", key=true}
                               { name: LIMIT, type="number:20,3", key=true}
                               ]
                       }
