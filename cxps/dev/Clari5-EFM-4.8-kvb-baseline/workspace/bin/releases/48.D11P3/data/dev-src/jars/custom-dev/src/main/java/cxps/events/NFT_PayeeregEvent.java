// -- ASSISTED CODE --
package cxps.events;

import java.util.Set;
import java.util.HashSet;
import java.util.Date;
import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonIgnore;

import clari5.platform.aef.AEF;
import clari5.platform.aef.annotations.Field;
import clari5.platform.aef.annotations.Table;
import clari5.platform.aef.features.ITable;
import clari5.platform.util.CxJson;
import clari5.adb.IArchivable;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.logger.CXLog;
import clari5.platform.logger.ICXLog;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;

import com.mysql.jdbc.StringUtils;
import cxps.apex.noesis.WorkspaceInfo;
import cxps.noesis.core.Event;
import cxps.noesis.core.EventHelper;


@Table(Name="EVENT_NFT_PAYEEREG", Schema="rice")
public class NFT_PayeeregEvent extends Event implements IArchivable {

    @Field(size = 100, key=true) public String eventId;
    @Field                       public Timestamp eventDate;
    @Field                       public Boolean isPostTransaction;
       @Field(size=20) public String country;
       @Field(size=20) public String payeeAccountId;
       @Field(size=20) public String payeeType;
       @Field(size=20) public String payeeNickname;
       @Field(size=20) public String payeeAccounttype;
       @Field(size=50) public String payeeBranchName;
       @Field(size=20) public String channel;
       @Field(size=50) public String errorDesc;
       @Field(size=2) public String succFailFlg;
       @Field(size=20) public String deviceId;
       @Field(size=50) public String payeeBankName;
       @Field(size=50) public String payeeAccountName;
       @Field(size=20) public String addrNetwork;
       @Field(size=20) public String payeeBranchId;
       @Field(size=20) public String payeeStatus;
       @Field(size=20) public Double payeeLimit;
       @Field(size=20) public String accountId;
       @Field(size=20) public String userId;
       @Field(size=50) public String payeeName;
       @Field(size=20) public String custId;
       @Field(size=20) public String errorCode;
       @Field(size=20) public String mfaType;
       @Field(size=20) public String networkType;
       @Field(size=20) public String payeeIfscCode;
       @Field(size=20) public String payeeBankId;
       @Field(size=20) public String payeeId;
       @Field(size=20) public String mobileNo;
       @Field(size=20) public String vpId;


    @JsonIgnore
    public ITable<NFT_PayeeregEvent> t = AEF.getITable(this);

	public NFT_PayeeregEvent(){}

    public NFT_PayeeregEvent(CxConnection con, Date from, Date to){
      this.t.setTblName(makeArchivable(this.t.getTableName(), con, from, to));
    }

    /**
    * This method is used to populate the event object using the data
    * coming in message body of event json from external system.
    */
    @Override
    public void from(CxJson json) {
        super.from(json);
        setEventType("NFT");
        setEventSubType("Payeereg");
        setEventName(getEventType() + "_" + getEventSubType());
        setEventId((!isPostTransaction() ? "RDA_" : "")+ json.getString("event_id"));

        if(getEventTS() != null)
            setEventDate(new Timestamp(getEventTS().getTime()));
        else
            setEventDate(new Timestamp(System.currentTimeMillis()));

            setCountry(json.getString("country"));
            setPayeeAccountId(json.getString("payee_account_id"));
            setPayeeType(json.getString("payee_type"));
            setPayeeNickname(json.getString("payee_nickname"));
            setPayeeAccounttype(json.getString("payee_accounttype"));
            setPayeeBranchName(json.getString("payee_branch_name"));
            setChannel(json.getString("channel"));
            setErrorDesc(json.getString("error_desc"));
            setSuccFailFlg(json.getString("succ_fail_flg"));
            setDeviceId(json.getString("device_id"));
            setPayeeBankName(json.getString("payee_bank_name"));
            setPayeeAccountName(json.getString("payee_account_name"));
            setAddrNetwork(json.getString("addr_network"));
            setPayeeBranchId(json.getString("payee_branch_id"));
            setPayeeStatus(json.getString("payee_status"));
            setPayeeLimit(EventHelper.toDouble(json.getString("payee_limit")));
            setAccountId(json.getString("account_id"));
            setUserId(json.getString("user_id"));
            setPayeeName(json.getString("payee_name"));
            setCustId(json.getString("cust_id"));
            setErrorCode(json.getString("error_code"));
            setMfaType(json.getString("mfa_type"));
            setNetworkType(json.getString("network_type"));
            setPayeeIfscCode(json.getString("payee_ifsc_code"));
            setPayeeBankId(json.getString("payee_bank_id"));
            setPayeeId(json.getString("payee_id"));
            setMobileNo(json.getString("mobile_no"));
            setVpId(json.getString("vp_id"));

        setDerivedValues();

    }


    private void setDerivedValues() {
    }


    /* Getters */
    @Override
    public String getMnemonic() { return "PG"; }

    public String getEventId() { return this.eventId;}
    public Timestamp getEventDate() { return this.eventDate; }
    public String getCountry(){ return country; }

    public String getPayeeAccountId(){ return payeeAccountId; }

    public String getPayeeType(){ return payeeType; }

    public String getPayeeNickname(){ return payeeNickname; }

    public String getPayeeAccounttype(){ return payeeAccounttype; }

    public String getPayeeBranchName(){ return payeeBranchName; }

    public String getChannel(){ return channel; }

    public String getErrorDesc(){ return errorDesc; }

    public String getSuccFailFlg(){ return succFailFlg; }

    public String getDeviceId(){ return deviceId; }

    public String getPayeeBankName(){ return payeeBankName; }

    public String getPayeeAccountName(){ return payeeAccountName; }

    public String getAddrNetwork(){ return addrNetwork; }

    public String getPayeeBranchId(){ return payeeBranchId; }

    public String getPayeeStatus(){ return payeeStatus; }

    public Double getPayeeLimit(){ return payeeLimit; }

    public String getAccountId(){ return accountId; }

    public String getUserId(){ return userId; }

    public String getPayeeName(){ return payeeName; }

    public String getCustId(){ return custId; }

    public String getErrorCode(){ return errorCode; }

    public String getMfaType(){ return mfaType; }

    public String getNetworkType(){ return networkType; }

    public String getPayeeIfscCode(){ return payeeIfscCode; }

    public String getPayeeBankId(){ return payeeBankId; }

    public String getPayeeId(){ return payeeId; }

    public String getMobileNo(){ return mobileNo; }

    public String getVpId(){ return vpId; }

    /* Setters */
    public void setEventId(String val) { this.eventId = val; }
    public void setEventDate(Timestamp val) { this.eventDate = val; }
    public void setCountry(String val){ this.country = val; }
    public void setPayeeAccountId(String val){ this.payeeAccountId = val; }
    public void setPayeeType(String val){ this.payeeType = val; }
    public void setPayeeNickname(String val){ this.payeeNickname = val; }
    public void setPayeeAccounttype(String val){ this.payeeAccounttype = val; }
    public void setPayeeBranchName(String val){ this.payeeBranchName = val; }
    public void setChannel(String val){ this.channel = val; }
    public void setErrorDesc(String val){ this.errorDesc = val; }
    public void setSuccFailFlg(String val){ this.succFailFlg = val; }
    public void setDeviceId(String val){ this.deviceId = val; }
    public void setPayeeBankName(String val){ this.payeeBankName = val; }
    public void setPayeeAccountName(String val){ this.payeeAccountName = val; }
    public void setAddrNetwork(String val){ this.addrNetwork = val; }
    public void setPayeeBranchId(String val){ this.payeeBranchId = val; }
    public void setPayeeStatus(String val){ this.payeeStatus = val; }
    public void setPayeeLimit(Double val){ this.payeeLimit = val; }
    public void setAccountId(String val){ this.accountId = val; }
    public void setUserId(String val){ this.userId = val; }
    public void setPayeeName(String val){ this.payeeName = val; }
    public void setCustId(String val){ this.custId = val; }
    public void setErrorCode(String val){ this.errorCode = val; }
    public void setMfaType(String val){ this.mfaType = val; }
    public void setNetworkType(String val){ this.networkType = val; }
    public void setPayeeIfscCode(String val){ this.payeeIfscCode = val; }
    public void setPayeeBankId(String val){ this.payeeBankId = val; }
    public void setPayeeId(String val){ this.payeeId = val; }
    public void setMobileNo(String val){ this.mobileNo = val; }
    public void setVpId(String val){ this.vpId = val; }

    /* Custom Getters*/


    /**
    * This method is used to return a set of WorkspaceInfo which contains the
    * information about the workspaces and it's corresponding entity which can be
    * derived for an event.
    */
    @Override
    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
        ICXLog cxLog = CXLog.fenter("derive.NFT_PayeeregEvent");
        Set<WorkspaceInfo> wsInfoSet = new HashSet<>();

        CxKeyHelper h = Hfdb.getCxKeyHelper();

        if (!this.accountId.equals("XXXXXX") && !this.accountId.equals("NA")) {
            if (!StringUtils.isNullOrEmpty(accountId)) {
                String accountKey = h.getCxKeyGivenHostKey(WorkspaceName.ACCOUNT, getHostId(), this.accountId);
                wsInfoSet.add(new WorkspaceInfo("Account", accountKey));
            }
        }
        String nonkey = this.payeeAccountId + this.payeeIfscCode;
        if (!nonkey.equals("XXXXXX") && !nonkey.equals("NA")) {
            if (!StringUtils.isNullOrEmpty(payeeAccountId + payeeIfscCode)) {
                String noncustomerKey = h.getCxKeyGivenHostKey(WorkspaceName.NONCUSTOMER, getHostId(), this.payeeAccountId + payeeIfscCode);
                wsInfoSet.add(new WorkspaceInfo("Noncustomer", noncustomerKey));
            }
        }

        if (!this.custId.equals("XXXXXX") && !this.custId.equals("NA")) {
            if (!StringUtils.isNullOrEmpty(custId)) {
                String customerKey = h.getCxKeyGivenHostKey(WorkspaceName.CUSTOMER, getHostId(), this.custId);
                wsInfoSet.add(new WorkspaceInfo("Customer", customerKey));
            }
        }

        cxLog.fexit();
        return wsInfoSet;
    }


    /**
    * This method is used to return a json to be shown as evidence on CMS (48.F+ release).
    */
    @JsonIgnore
    @Override
    public CxJson getEvidenceAsJson() {
        CxJson json = new CxJson();
        json.put("host_id", getHostId());
        return json;
    }

    /**
    * This method is used to return a json to be used in Fraud Resolution.
    * The fields present in this json are the fields which were marked with 'fr:true' in
    * the event spec along with some mandatory fields like : event_id, event_name, event_type, event_sub_type, sys_time.
    */
    @JsonIgnore
    @Override
    public CxJson getFRAsJson() {
        CxJson json = new CxJson();
        json.put("event_id", getEventId());
        json.put("event_name", "NFT_Payeereg");
        json.put("event_type", "NFT");
        json.put("event_sub_type", "Payeereg");
        if(getEventTS() != null) json.put("sys_time", getEventTS().getTime());
        if (!StringUtils.isNullOrEmpty(getPayeeAccountId())) {
            json.put("payee_account_id",getPayeeAccountId());
        }
        else
            json.put("payee_account_id", "XXXXXX");

        if (!StringUtils.isNullOrEmpty(getAccountId())) {
            json.put("account_id",getAccountId());
        }
        else
            json.put("account_id", "XXXXXX");

        if (!StringUtils.isNullOrEmpty(getPayeeIfscCode())) {
            json.put("payee_ifsc_code",getPayeeIfscCode());
        }
        else
            json.put("payee_ifsc_code", "XXXXXX");

        if (!StringUtils.isNullOrEmpty(getCustId())) {
            json.put("cust_id", getCustId());
        }
        else
            json.put("cust_id", "XXXXXX");
        return json;
    }


    /**
    * This method is supposed to return a proper user understandable
    * message to be shown on the UI for end user.
    * For ex: for FT_AccountTxnEvent the message can be -> User has debited Rs 10000/- from
    * his account on 10th Feb, 2017 at 01:00 pm
    */
    @JsonIgnore
    @Override
    public String getEvidenceMessage(String factname) {
        String defaultMessage = ""; //TODO to be implemented by the developer.
        String message = EventHelper.evidenceMessage(this, factname);
        if(message != null && !"".equals(message)) return message;
        return defaultMessage;
    }
}