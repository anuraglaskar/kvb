package clari5.custom.dev.services.sendSms;

import clari5.custom.dev.services.SmsemailutilityException;
import clari5.custom.dev.services.StrSubstitutor;
import clari5.platform.util.Hocon;
import clari5.platform.logger.CxpsLogger;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Soapcon {
	private static CxpsLogger logger = CxpsLogger.getLogger(Soapcon.class);
	private static String ssl = "";
	private static int readTimeout = 5000;
	private static int conTimeout = 5000;
	private static String url = "";
	private static String requestProperty = "";
	private static String smsDataRaw = "";
	private static Hocon conf = null;
	private static Hocon smsDataFile = null;
	static {
		conf = new Hocon();
		smsDataFile = new Hocon();
		smsDataFile.loadFromContext("sms-data.properties");
		conf.loadFromContext("sms-details.conf");
		if(!conf.getString("sms.readtimeout").isEmpty())
			readTimeout = conf.getInt("sms.readtimeout");
		if(!conf.getString("sms.contimeout").isEmpty())
			conTimeout = conf.getInt("sms.contimeout");
		ssl = conf.getString("ssl");
		url = conf.getString("wsdl.url");
		requestProperty = conf.getString("requestproperty");
		smsDataRaw = smsDataFile.getString("data");
	}
	public String sendSoapReqFT(String mobileNum, String tranDate, String custAct, String issue_key, String respTag ) {
		//System.out.println(mobileNum+" "+tranDate+" "+custAct+" "+issue_key+" "+respTag);
		Map<String, String> placeHolders = new HashMap<String, String>();
		placeHolders.put("account_id", custAct);
		placeHolders.put("tran_date", tranDate);
		placeHolders.put("cust_mob_num", mobileNum);

		StrSubstitutor strSubstitutor = new StrSubstitutor(placeHolders);
		String smsData = strSubstitutor.replace(smsDataRaw);
		//System.out.println(smsData);

		StringBuilder response = new StringBuilder();
		try {
			 URL obj = new URL(url);
			 HttpsURLConnection con_https = null;
			 HttpURLConnection con_http = null;
			 DataOutputStream wr = null;
			 if(ssl.equals("yes")) {
			 	con_https = (HttpsURLConnection) obj.openConnection();
			 	con_https.setRequestMethod("POST");
			 	con_https.setDoOutput(true);
			 	con_https.setReadTimeout(readTimeout);
			 	con_https.setConnectTimeout(conTimeout);
			 }
			 else {
				 con_http = (HttpsURLConnection) obj.openConnection();
				 con_http.setRequestMethod("POST");
				 con_http.setDoOutput(true);
				 con_http.setReadTimeout(readTimeout);
				 con_http.setConnectTimeout(conTimeout);
			 }
			 /*
			 Bank's SOAP API
			  */
			 if(requestProperty.equals("Content-Type")) {
				logger.info("Sending request to SMS Endpoint");
			 	if(ssl.equals("yes")) {
					con_https.setRequestProperty("Content-Type", "application/text; charset=utf-8");
					con_https.setRequestProperty("SOAPAction","");
					wr = new DataOutputStream(con_https.getOutputStream());
				}
			 	else {
					con_http.setRequestProperty("Content-Type", "application/text; charset=utf-8");
					con_http.setRequestProperty("SOAPAction","");
					wr = new DataOutputStream(con_http.getOutputStream());
				}
			 	wr.writeBytes(smsData);
			 	wr.flush();
			 	wr.close();
			 }
			 /*
				Testing
			  */
			 if(requestProperty.equals("Content-Length")) {
				 //System.out.println("CALLING TESTING SMS API");
				 logger.info("Calling Testing API");
				 if(ssl.equals("yes")) {
					 con_https.setRequestProperty("Content-Length", Integer.toString(smsData.length()));
					 con_https.getOutputStream().write(smsData.getBytes("UTF-8"));
				 }
				 else {
					 con_http.setRequestProperty("Content-Length", Integer.toString(smsData.length()));
					 con_http.getOutputStream().write(smsData.getBytes("UTF-8"));
				 }
			 }
			 //GETTING THE RESPONSE BODY
			BufferedReader in =  null;
			 if(ssl.equals("yes")){
			 	in = new BufferedReader(new InputStreamReader(
						 con_https.getInputStream()));
			 }
			 else {
			 	in = new BufferedReader(new InputStreamReader(
						 con_http.getInputStream()));
			 }
			 String inputLine;
			 while ((inputLine = in.readLine()) != null) {
				 response.append(inputLine);
			 }
			 in.close();

		} catch (Exception e) {
			//System.out.println("Couldn't send SMS");
			logger.error("Failed to send SMS");
			e.printStackTrace();
			return "<"+respTag+">Connection Failure</"+respTag+">";

		}
		return response.toString();
	}
	public  String stringParser(String response, String responseTag){
		try {
			String rgex = "<" + responseTag + ">[a-z,A-Z,0-9, \\s]{0,}";
			Pattern pattern = Pattern.compile(rgex);
			Matcher matcher = pattern.matcher(response);
			String res = "";
			while (matcher.find()) {
				res = matcher.group();
			}
			res = res.replace("<" + responseTag + ">", "");
			return res;
		}catch (Exception e) {
			e.printStackTrace();
			return "RESPONSE ERROR";
		}
	}

}
