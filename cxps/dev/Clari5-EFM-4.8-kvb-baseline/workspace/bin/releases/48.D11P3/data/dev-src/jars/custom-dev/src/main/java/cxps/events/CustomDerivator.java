package cxps.events;

import clari5.rdbms.Rdbms;
import cxps.apex.utils.StringUtils;

import java.sql.*;

public class CustomDerivator {

    private static String countryCode;

    public static String getIsPoolAcct(NFT_DbloginEvent event){
        Connection con = null;
        PreparedStatement psmt = null;
        ResultSet result = null;
        String isPoolAcct = "N";
        if (StringUtils.isNullOrEmpty(event.getAccountId()) && event.getAccountId().equals("NA")){
            isPoolAcct = "N";
            return isPoolAcct;
        }
        try {
            con = Rdbms.getAppConnection();
            String query = "SELECT ACCOUNT_ID from POOL_ACCOUNT WHERE ACCOUNT_ID = '" + event.getAccountId() + "'";
            psmt = con.prepareStatement(query);
            result = psmt.executeQuery();

            while (result.next()) {
                isPoolAcct = "Y";
            }
        }
             catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            try {
                if (psmt != null) {
                    psmt.close();
                }
                if (con != null) {
                    con.close();
                }
                if (result != null) {
                    result.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return isPoolAcct;
    }

    public static String getIsPoolAcct(FT_FundstransferEvent event){
        Connection con = null;
        PreparedStatement psmt = null;
        ResultSet result = null;
        String isPoolAcct = "N";
        if (StringUtils.isNullOrEmpty(event.getAccountId()) && event.getAccountId().equals("NA")){
            isPoolAcct = "N";
            return isPoolAcct;
        }
        try {
            con = Rdbms.getAppConnection();
            String query = "SELECT ACCOUNT_ID from POOL_ACCOUNT WHERE ACCOUNT_ID = '" + event.getAccountId() + "'";
            psmt = con.prepareStatement(query);
            result = psmt.executeQuery();

                while (result.next()) {
                    isPoolAcct = "Y";
                }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            try {
                if (psmt != null) {
                    psmt.close();
                }
                if (con != null) {
                    con.close();
                }
                if (result != null) {
                    result.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return isPoolAcct;
    }


    public static String getIsRecPoolAcct(FT_FundstransferEvent event){
        Connection con = null;
        PreparedStatement psmt = null;
        ResultSet result = null;
        String isREcPoolAcct = "N";
        if (StringUtils.isNullOrEmpty(event.getPayeeAcctId()) && event.getPayeeAcctId().equals("NA")){
            isREcPoolAcct = "N";
            return isREcPoolAcct;
        }
        try {
            con = Rdbms.getAppConnection();
            String query = "SELECT ACCOUNT_ID from POOL_ACCOUNT WHERE ACCOUNT_ID = '" + event.getPayeeAcctId() + "'";
            psmt = con.prepareStatement(query);
            result = psmt.executeQuery();

            while (result.next()) {
                isREcPoolAcct = "Y";
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            try {
                if (psmt != null) {
                    psmt.close();
                }
                if (con != null) {
                    con.close();
                }
                if (result != null) {
                    result.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return isREcPoolAcct;
    }

    public static String getCustName(FT_FundstransferEvent event){
        ResultSet result = null;
        String custName="";
        Connection con = Rdbms.getAppConnection();
        Statement st = null;
        try
        {
            String query2 = "select \"custName\" from customer where \"hostCustId\" = '" + event.getCustId() + "'";
            st = con.createStatement();
            result = st.executeQuery(query2);
            while(result.next()) {
               // System.out.println("Customer name is :" + result.getString("custName"));
                if(!StringUtils.isNullOrEmpty(result.getString( "custName")))
                    // event.setIsStaff(result.getString("staff"));
                    custName = result.getString("custName");
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try {
                if (st != null) {
                    st.close();
                }

                if (con != null) {
                    con.close();
                }
                if(result !=null){
                    result.close();
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return custName;
    }

    public static String getCustName(NFT_DbloginEvent event){
        ResultSet result = null;
        String custName="";
        Connection con = Rdbms.getAppConnection();
        Statement st = null;
        try
        {
            String query2 = "select \"custName\" from customer where \"hostCustId\" = '" + event.getCustId() + "'";
            st = con.createStatement();
            result = st.executeQuery(query2);
            while(result.next()) {
                //System.out.println("Customer Name is :" + result.getString("custName"));
                if(!StringUtils.isNullOrEmpty(result.getString( "custName")))
                    // event.setIsStaff(result.getString("staff"));
                    custName = result.getString("custName");
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try {
                if (st != null) {
                    st.close();
                }

                if (con != null) {
                    con.close();
                }
                if(result !=null){
                    result.close();
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return custName;
    }

    public static String getCustMobile(FT_FundstransferEvent event) {
        ResultSet result = null;
        String custMobile = "";
        Connection con = Rdbms.getAppConnection();
        Statement st = null;
        try {
            String query2 = "select \"custMobile1\" from customer where \"hostCustId\" = '" + event.getCustId() + "'";
            st = con.createStatement();
            result = st.executeQuery(query2);
            while (result.next()) {
                //System.out.println("Mobile no is :" + result.getString("custMobile1"));
                if (!StringUtils.isNullOrEmpty(result.getString("custMobile1")))
                    // event.setIsStaff(result.getString("staff"));
                    custMobile = result.getString("custMobile1");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (st != null) {
                    st.close();
                }

                if (con != null) {
                    con.close();
                }
                if (result != null) {
                    result.close();
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }

        }
        return custMobile;
    }

    public static String getCustMobile(NFT_DbloginEvent event){
        ResultSet result = null;
        String custMobile="";
        Connection con = Rdbms.getAppConnection();
        Statement st = null;
        try
        {
            String query2 = "select \"custMobile1\" from customer where \"hostCustId\" = '" + event.getCustId() + "'";
            st = con.createStatement();
            result = st.executeQuery(query2);
            while(result.next()) {
                //System.out.println("Mobile no is :" + result.getString("custMobile1"));
                if(!StringUtils.isNullOrEmpty(result.getString( "custMobile1")))
                    // event.setIsStaff(result.getString("staff"));
                    custMobile = result.getString("custMobile1");
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try {
                if (st != null) {
                    st.close();
                }

                if (con != null) {
                    con.close();
                }
                if(result !=null){
                    result.close();
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }

        }
        return custMobile;
    }

    public static String getCountryCode(FT_FundstransferEvent nft){

        String ip = nft.getIpAddress();
        if (!StringUtils.isNullOrEmpty(ip)) {
            //System.out.println("The ip address retrieved is: " + ip);
            IPDerivator ipDerivator = new IPDerivator();
            String ipLocation = ipDerivator.getLocation(ip);
            if ((ipLocation != null)) {
                nft.setCountryCode(ipLocation);
                //System.out.println("Setting ipcountry as country code : " + ipLocation.countryCode);
            }
        }
        return nft.countryCode;

    }


 /*   public static String getCountryCode(FT_FundstransferEvent nft){

        String ip = nft.getIpAddress();
        if (!StringUtils.isNullOrEmpty(ip)) {
            //System.out.println("The ip address retrieved is: " + ip);
            IPDerivator ipDerivator = new IPDerivator();
            Location ipLocation = ipDerivator.getLocation(ip);
            if ((ipLocation != null) && (ipLocation.countryCode != null)) {
                nft.setCountryCode(ipLocation.countryCode);
                //System.out.println("Setting ipcountry as country code : " + ipLocation.countryCode);
            }
        }
        return nft.countryCode;

    }

*/



    public static String getCountryCode(NFT_DbloginEvent nft){

        String ip = nft.getAddrNetwork();
        if (!StringUtils.isNullOrEmpty(ip)) {
           // System.out.println("The ip address retrieved is: " + ip);
            IPDerivator ipDerivator = new IPDerivator();
            String ipLocation = ipDerivator.getLocation(ip);
            if ((ipLocation != null)) {
                nft.setCountryCode(ipLocation);
                //System.out.println("Setting ipcountry as country code : " + ipLocation.countryCode);
            }
        }
        return nft.countryCode;

    }

    public static Double getAvlBal(FT_FundstransferEvent event){
        Connection con = null;
        Statement stmt = null;
        Double avlBal = 0.0;
        try {
            con = Rdbms.getAppConnection();
            stmt = con.createStatement();
            String query = "SELECT \"AVLBAL\" from DDA_ADDTLS WHERE \"ACCOUNT_NO\"  = '" + event.getAccountId() + "'";

            ResultSet balance = stmt.executeQuery(query);
            while (balance.next())
                avlBal = balance.getDouble("AVLBAL");
            if(avlBal == null)
                avlBal = 0.0;
        }
        catch (SQLException e) {
            e.printStackTrace();
        }

        finally {
            try {
                if (con != null) {
                    con.close();
                }
                if ( stmt != null) {
                    stmt.close();
                }
            }
            catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return avlBal;
    }

    public static Timestamp getLastIndDate(FT_FundstransferEvent event){
        Connection con = null;
        Timestamp induceDate = null;
        long sysTime = System.currentTimeMillis();
        Timestamp ts = new Timestamp(sysTime);
        Statement stmt = null;
        try {
            con = Rdbms.getAppConnection();
            stmt = con.createStatement();
            String query = "SELECT \"LAST_CUST_INDUCED_DT\" from DDA_ADDTLS WHERE \"ACCOUNT_NO\"  = '" + event.getAccountId() + "'";
            ResultSet balance = stmt.executeQuery(query);
            while (balance.next())
                induceDate = balance.getTimestamp("LAST_CUST_INDUCED_DT");
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
                finally {
            try {
                if (con != null) {
                    con.close();
                }
                if ( stmt != null) {
                    stmt.close();
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
            if (induceDate == null || induceDate.equals(""))
                return ts;
            else
                return induceDate;

        }

    }

