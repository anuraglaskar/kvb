package clari5.custom.dev.DbUtil;

import clari5.rdbms.Rdbms;
import clari5.platform.logger.CxpsLogger;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class Dbfetch {
	private static CxpsLogger logger = CxpsLogger.getLogger(Dbfetch.class);
	private String custPhoneNumber1;
	private String custPhoneNumber2;
	private String custEmail1;
	private String custEmail2;
	private String custId;
	private String tranDate;
	private String custAcctId;
	private String eventId;
	private List<String> tranDetails;
//	private String jdbcUrl = "";
//	private String dbUserName = "";
//	private String dbPassWord = "";
	ResultSet rs;
	Connection conn;
	PreparedStatement ps;

	public Dbfetch() {
		this.custPhoneNumber1 = "";
		this.custPhoneNumber2 = "";
		this.custEmail1 = "";
		this.custEmail2 = "";
		this.custId = "";
		this.custAcctId = "";
		this.tranDate = "";
		this.eventId = "";
		this.tranDetails = new ArrayList<String>();
//		this.jdbcUrl = jdbcUrl;
//		this.dbUserName = dbUserName;
//		this.dbPassWord = dbPassWord;
		this.rs = null;
		this.conn = null;
		this.ps = null;
	}

	public String getCustPhoneNum(String custId) {

		try {
//			Class.forName("oracle.jdbc.driver.OracleDriver");
//			conn = DriverManager.getConnection(jdbcUrl, dbUserName, dbPassWord);
			conn = Rdbms.getConnection();
			String query = "SELECT \"custMobile1\", \"custMobile2\" from CUSTOMER where \"hostCustId\"=?";
			ps = conn.prepareStatement(query);
			ps.setString(1, custId);
			rs = ps.executeQuery();
			while (rs.next()) {
				if(rs.getString("custMobile1") != null && rs.getString("custMobile1").length() > 1)
					custPhoneNumber1 = rs.getString("custMobile1");
				if(rs.getString("custMobile2") != null && rs.getString("custMobile2").length() > 1)
					custPhoneNumber2 = rs.getString("custMobile2");
				//System.out.println(rs.getString(1)+" "+rs.getString(2));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		if(custPhoneNumber1 != null)
			return custPhoneNumber1;
		else
			return custPhoneNumber2;
	}

	public String getCustEmail(String custId) {
		try {
//			Class.forName("oracle.jdbc.driver.OracleDriver");
//			conn = DriverManager.getConnection(jdbcUrl, dbUserName, dbPassWord);
			conn = Rdbms.getConnection();
			String query = "SELECT \"custEmail1\", \"custEmail2\" from CUSTOMER where \"hostCustId\"=?";
			ps = conn.prepareStatement(query);
			ps.setString(1, custId);
			rs = ps.executeQuery();
			while (rs.next()) {
				if(rs.getString("custEmail1") != null && rs.getString("custEmail1").length() > 1)
					custEmail1 = rs.getString("custEmail1");
				if(rs.getString("custEmail2") != null && rs.getString("custEmail2").length() > 1)
					custEmail2 = rs.getString("custEmail2");
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		if(custEmail1 != null)
			return custEmail1;
		else
			return custEmail2;
	}

	public List<String> getTranDetails(String issue_key) {
		List<String> eventDetails = getEventType(issue_key);
		String eventType = eventDetails.get(1);
		eventId = eventDetails.get(0);
		if(eventType.equals("FUND")){
			try {
//			Class.forName("oracle.jdbc.driver.OracleDriver");
//			conn = DriverManager.getConnection(jdbcUrl, dbUserName, dbPassWord);
				conn = Rdbms.getConnection();
				String query = "SELECT CUST_ID, ACCOUNT_ID, EVENT_DATE FROM EVENT_FT_FUNDSTRANSFER WHERE EVENT_ID=?";
				ps = conn.prepareStatement(query);
				ps.setString(1, eventId);
				rs = ps.executeQuery();
				while (rs.next()) {
					custId = rs.getString("CUST_ID");
					custAcctId = rs.getString("ACCOUNT_ID");
					tranDate = rs.getString("EVENT_DATE");
				}
				tranDetails.add(custId);
				tranDetails.add(custAcctId);
				tranDetails.add(tranDate);
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				try {
					if (conn != null) {
						conn.close();
					}

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			return tranDetails;
		}
		else if(eventType.equals("FFT")) {
			try {
//			Class.forName("oracle.jdbc.driver.OracleDriver");
//			conn = DriverManager.getConnection(jdbcUrl, dbUserName, dbPassWord);
				conn = Rdbms.getConnection();
				String query = "SELECT CUST_ID, ACCOUNT_ID, EVENT_DATE FROM EVENT_FT_FAILEDFUNDSTRANSFER WHERE EVENT_ID=?";
				ps = conn.prepareStatement(query);
				ps.setString(1, eventId);
				rs = ps.executeQuery();
				while (rs.next()) {
					custId = rs.getString("CUST_ID");
					custAcctId = rs.getString("ACCOUNT_ID");
					tranDate = rs.getString("EVENT_DATE");
				}
				tranDetails.add(custId);
				tranDetails.add(custAcctId);
				tranDetails.add(tranDate);
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				try {
					if (conn != null) {
						conn.close();
					}

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			return tranDetails;
		}
		else if(eventType.equals("LN")) {
			try {
//			Class.forName("oracle.jdbc.driver.OracleDriver");
//			conn = DriverManager.getConnection(jdbcUrl, dbUserName, dbPassWord);
				conn = Rdbms.getConnection();
				String query = "SELECT CUST_ID, ACCOUNT_ID, EVENT_DATE FROM EVENT_NFT_DBLOGIN WHERE EVENT_ID=?";
				ps = conn.prepareStatement(query);
				ps.setString(1, eventId);
				rs = ps.executeQuery();
				while (rs.next()) {
					custId = rs.getString("CUST_ID");
					custAcctId = rs.getString("ACCOUNT_ID");
					tranDate = rs.getString("EVENT_DATE");
				}
				tranDetails.add(custId);
				tranDetails.add(custAcctId);
				tranDetails.add(tranDate);
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				try {
					if (conn != null) {
						conn.close();
					}

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			return tranDetails;
		}
		else if(eventType.equals("IPC")) {
			try {
//			Class.forName("oracle.jdbc.driver.OracleDriver");
//			conn = DriverManager.getConnection(jdbcUrl, dbUserName, dbPassWord);
				conn = Rdbms.getConnection();
				String query = "SELECT CUST_ID, ACCOUNT_ID, EVENT_DATE FROM EVENT_NFT_IBPWDCNG WHERE EVENT_ID=?";
				ps = conn.prepareStatement(query);
				ps.setString(1, eventId);
				rs = ps.executeQuery();
				while (rs.next()) {
					custId = rs.getString("CUST_ID");
					custAcctId = rs.getString("ACCOUNT_ID");
					tranDate = rs.getString("EVENT_DATE");
				}
				tranDetails.add(custId);
				tranDetails.add(custAcctId);
				tranDetails.add(tranDate);
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				try {
					if (conn != null) {
						conn.close();
					}

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			return tranDetails;
		}
		else  if(eventType.equals("MPR")) {
			try {
//			Class.forName("oracle.jdbc.driver.OracleDriver");
//			conn = DriverManager.getConnection(jdbcUrl, dbUserName, dbPassWord);
				conn = Rdbms.getConnection();
				String query = "SELECT CUST_ID, ACCOUNT_ID, EVENT_DATE FROM EVENT_NFT_MBPINREGEN WHERE EVENT_ID=?";
				ps = conn.prepareStatement(query);
				ps.setString(1, eventId);
				rs = ps.executeQuery();
				while (rs.next()) {
					custId = rs.getString("CUST_ID");
					custAcctId = rs.getString("ACCOUNT_ID");
					tranDate = rs.getString("EVENT_DATE");
				}
				tranDetails.add(custId);
				tranDetails.add(custAcctId);
				tranDetails.add(tranDate);
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				try {
					if (conn != null) {
						conn.close();
					}

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			return tranDetails;
		}
		else if(eventType.equals("PG")) {
			try {
//			Class.forName("oracle.jdbc.driver.OracleDriver");
//			conn = DriverManager.getConnection(jdbcUrl, dbUserName, dbPassWord);
				conn = Rdbms.getConnection();
				String query = "SELECT CUST_ID, ACCOUNT_ID, EVENT_DATE FROM EVENT_NFT_PAYEEREG WHERE EVENT_ID=?";
				ps = conn.prepareStatement(query);
				ps.setString(1, eventId);
				rs = ps.executeQuery();
				while (rs.next()) {
					custId = rs.getString("CUST_ID");
					custAcctId = rs.getString("ACCOUNT_ID");
					tranDate = rs.getString("EVENT_DATE");
				}
				tranDetails.add(custId);
				tranDetails.add(custAcctId);
				tranDetails.add(tranDate);
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				try {
					if (conn != null) {
						conn.close();
					}

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			return tranDetails;
		}
		else if(eventType.equals("RG")) {
			try {
//			Class.forName("oracle.jdbc.driver.OracleDriver");
//			conn = DriverManager.getConnection(jdbcUrl, dbUserName, dbPassWord);
				conn = Rdbms.getConnection();
				String query = "SELECT CUST_ID, ACCOUNT_ID, EVENT_DATE FROM EVENT_NFT_UPIREG WHERE EVENT_ID=?";
				ps = conn.prepareStatement(query);
				ps.setString(1, eventId);
				rs = ps.executeQuery();
				while (rs.next()) {
					custId = rs.getString("CUST_ID");
					custAcctId = rs.getString("ACCOUNT_ID");
					tranDate = rs.getString("EVENT_DATE");
				}
				tranDetails.add(custId);
				tranDetails.add(custAcctId);
				tranDetails.add(tranDate);
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				try {
					if (conn != null) {
						conn.close();
					}

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			return tranDetails;
		}
		return  tranDetails;
	}

	private List<String> getEventType(String issue_key) {
		List<String> eventDetails = new ArrayList<String>();
		String query = "SELECT SUBSTR(TRIGGER_ID, 0, REGEXP_INSTR(TRIGGER_ID, FACT_NAME ) - 1) FROM CL5_INCIDENT WHERE JIRA_INCIDENTID=? ORDER BY OPENED_DATE DESC FETCH FIRST 1 ROW ONLY";
		String eventid = "";
		String eventtype = "";
		try {
			conn = Rdbms.getConnection();
			ps = conn.prepareStatement(query);
			ps.setString(1, issue_key);
			rs = ps.executeQuery();
			while (rs.next())
				eventid = rs.getString(1);
			if(eventid.substring(0,4).equals("FUND"))
				eventtype = "FUND";
			else if(eventid.substring(0, 3).equals("FFT"))
				eventtype = "FFT";
			else if(eventid.substring(0, 2).equals("LN"))
				eventtype = "LN";
			else if(eventid.substring(0, 3).equals("IPC"))
				eventtype = "IPC";
			else if(eventid.substring(0, 3).equals("MPR"))
				eventtype = "MPR";
			else if(eventid.substring(0, 2).equals("PG"))
				eventtype = "PG";
			else if(eventid.substring(0, 2).equals("RG"))
				eventtype = "RG";
		}catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if (conn != null)
					conn.close();
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
		eventDetails.add(eventid);
		eventDetails.add(eventtype);
		//System.out.println(eventid+" "+eventtype);
		return eventDetails;
	}

	public String updateSmsEmailDetailsTable(String type, String emailId, String mobNum, String custId, String response){
		logger.info("UPDATING EMAIL_SMS_DETAILS TABLE");
		String email = "EMAIL";
		String sms = "SMS";
		if(type.equals(sms)){
			//System.out.println("SMS");
			String cust_id = "";
			String query1 = "SELECT CUSTOMERID FROM EMAIL_SMS_DETAILS WHERE CUSTOMERID=?";
			try {
				conn = Rdbms.getConnection();
				ps = conn.prepareStatement(query1);
				ps.setString(1,custId);
				rs = ps.executeQuery();
				while (rs.next()){
					cust_id = rs.getString(1);
				}
				if(cust_id.length() == 0){
					String query2 = "INSERT INTO EMAIL_SMS_DETAILS VALUES(?,?,?,?)";
					ps = conn.prepareStatement(query2);
					ps.setString(1, custId);
					ps.setString(2, emailId);
					ps.setString(3, mobNum);
					ps.setString(4, response);
					ps.executeQuery();
					conn.commit();
				}
				else {
					String query3 = "UPDATE EMAIL_SMS_DETAILS SET MOBILENUM=?,RESPONSE=? WHERE CUSTOMERID=?";
					ps = conn.prepareStatement(query3);
					ps.setString(1, mobNum);
					ps.setString(2, response);
					ps.setString(3, custId);
					ps.executeQuery();
					conn.commit();
				}
			}catch (SQLException e){
				e.printStackTrace();
			}
			finally {
				try {
					if(conn != null) {
						conn.close();
					}
				}catch (SQLException e){
					e.printStackTrace();
				}
			}
		}
		if(type.equals(email)){
			//System.out.println("EMAIL");
			String cust_id = "";
			String query1 = "SELECT CUSTOMERID FROM EMAIL_SMS_DETAILS WHERE CUSTOMERID=?";
			try {
				conn = Rdbms.getConnection();
				ps = conn.prepareStatement(query1);
				ps.setString(1,custId);
				rs = ps.executeQuery();
				while (rs.next()){
					cust_id = rs.getString(1);
				}
				if(cust_id.length() == 0){
					String query2 = "INSERT INTO EMAIL_SMS_DETAILS VALUES(?,?,?,?)";
					ps = conn.prepareStatement(query2);
					ps.setString(1, custId);
					ps.setString(2, emailId);
					ps.setString(3, mobNum);
					ps.setString(4, response);
					ps.executeQuery();
					conn.commit();
				}
				else {
					String query3 = "UPDATE EMAIL_SMS_DETAILS SET EMAILID=?,RESPONSE=? WHERE CUSTOMERID=?";
					ps = conn.prepareStatement(query3);
					ps.setString(1, emailId);
					ps.setString(2, response);
					ps.setString(3, custId);
					ps.executeQuery();
					conn.commit();
				}
			}catch (SQLException e){
				e.printStackTrace();
			}finally {
				try{
					if(conn != null){
						conn.close();
					}
				}catch (SQLException e){
					e.printStackTrace();
				}
			}
		}
		return "Successfully stored the records";
	}
}
