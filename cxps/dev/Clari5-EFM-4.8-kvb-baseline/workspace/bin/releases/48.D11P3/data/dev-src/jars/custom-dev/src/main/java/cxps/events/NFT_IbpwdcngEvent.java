// -- ASSISTED CODE --
package cxps.events;

import java.util.Set;
import java.util.HashSet;
import java.util.Date;
import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonIgnore;

import clari5.platform.aef.AEF;
import clari5.platform.aef.annotations.Field;
import clari5.platform.aef.annotations.Table;
import clari5.platform.aef.features.ITable;
import clari5.platform.util.CxJson;
import clari5.adb.IArchivable;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.logger.CXLog;
import clari5.platform.logger.ICXLog;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;

import com.mysql.jdbc.StringUtils;
import cxps.apex.noesis.WorkspaceInfo;
import cxps.noesis.core.Event;
import cxps.noesis.core.EventHelper;


@Table(Name="EVENT_NFT_IBPWDCNG", Schema="rice")
public class NFT_IbpwdcngEvent extends Event implements IArchivable {

    @Field(size = 100, key=true) public String eventId;
    @Field                       public Timestamp eventDate;
    @Field                       public Boolean isPostTransaction;
       @Field(size=20) public String country;
       @Field(size=20) public String corporateId;
       @Field(size=20) public String ipCity;
       @Field(size=20) public String cngMode;
       @Field(size=20) public String accountId;
       @Field(size=20) public String userId;
       @Field(size=50) public String errorDesc;
       @Field(size=20) public String channel;
       @Field(size=2) public String succFailFlg;
       @Field(size=20) public String errorCode;
       @Field(size=20) public String ipCountry;
       @Field(size=20) public String custId;
       @Field(size=20) public String type;
       @Field(size=20) public String deviceId;
       @Field(size=20) public String resetUsing;
       @Field(size=20) public String addrNetwork;
       @Field(size=20) public String deviceType;
       @Field(size=20) public String mobileNo;


    @JsonIgnore
    public ITable<NFT_IbpwdcngEvent> t = AEF.getITable(this);

	public NFT_IbpwdcngEvent(){}

    public NFT_IbpwdcngEvent(CxConnection con, Date from, Date to){
      this.t.setTblName(makeArchivable(this.t.getTableName(), con, from, to));
    }

    /**
    * This method is used to populate the event object using the data
    * coming in message body of event json from external system.
    */
    @Override
    public void from(CxJson json) {
        super.from(json);
        setEventType("NFT");
        setEventSubType("Ibpwdcng");
        setEventName(getEventType() + "_" + getEventSubType());
        setEventId((!isPostTransaction() ? "RDA_" : "")+ json.getString("event_id"));

        if(getEventTS() != null)
            setEventDate(new Timestamp(getEventTS().getTime()));
        else
            setEventDate(new Timestamp(System.currentTimeMillis()));

            setCountry(json.getString("country"));
            setCorporateId(json.getString("corporate_id"));
            setIpCity(json.getString("ip_city"));
            setCngMode(json.getString("cng_mode"));
            setAccountId(json.getString("account_id"));
            setUserId(json.getString("user_id"));
            setErrorDesc(json.getString("error_desc"));
            setChannel(json.getString("channel"));
            setSuccFailFlg(json.getString("succ_fail_flg"));
            setErrorCode(json.getString("error_code"));
            setIpCountry(json.getString("ip_country"));
            setCustId(json.getString("cust_id"));
            setType(json.getString("type"));
            setDeviceId(json.getString("device_id"));
            setResetUsing(json.getString("reset_using"));
            setAddrNetwork(json.getString("addr_network"));
            setDeviceType(json.getString("device_type"));
            setMobileNo(json.getString("mobile_no"));

        setDerivedValues();

    }


    private void setDerivedValues() {
    }


    /* Getters */
    @Override
    public String getMnemonic() { return "IPC"; }

    public String getEventId() { return this.eventId;}
    public Timestamp getEventDate() { return this.eventDate; }
    public String getCountry(){ return country; }

    public String getCorporateId(){ return corporateId; }

    public String getIpCity(){ return ipCity; }

    public String getCngMode(){ return cngMode; }

    public String getAccountId(){ return accountId; }

    public String getUserId(){ return userId; }

    public String getErrorDesc(){ return errorDesc; }

    public String getChannel(){ return channel; }

    public String getSuccFailFlg(){ return succFailFlg; }

    public String getErrorCode(){ return errorCode; }

    public String getIpCountry(){ return ipCountry; }

    public String getCustId(){ return custId; }

    public String getType(){ return type; }

    public String getDeviceId(){ return deviceId; }

    public String getResetUsing(){ return resetUsing; }

    public String getAddrNetwork(){ return addrNetwork; }

    public String getDeviceType(){ return deviceType; }

    public String getMobileNo(){ return mobileNo; }

    /* Setters */
    public void setEventId(String val) { this.eventId = val; }
    public void setEventDate(Timestamp val) { this.eventDate = val; }
    public void setCountry(String val){ this.country = val; }
    public void setCorporateId(String val){ this.corporateId = val; }
    public void setIpCity(String val){ this.ipCity = val; }
    public void setCngMode(String val){ this.cngMode = val; }
    public void setAccountId(String val){ this.accountId = val; }
    public void setUserId(String val){ this.userId = val; }
    public void setErrorDesc(String val){ this.errorDesc = val; }
    public void setChannel(String val){ this.channel = val; }
    public void setSuccFailFlg(String val){ this.succFailFlg = val; }
    public void setErrorCode(String val){ this.errorCode = val; }
    public void setIpCountry(String val){ this.ipCountry = val; }
    public void setCustId(String val){ this.custId = val; }
    public void setType(String val){ this.type = val; }
    public void setDeviceId(String val){ this.deviceId = val; }
    public void setResetUsing(String val){ this.resetUsing = val; }
    public void setAddrNetwork(String val){ this.addrNetwork = val; }
    public void setDeviceType(String val){ this.deviceType = val; }
    public void setMobileNo(String val){ this.mobileNo = val; }

    /* Custom Getters*/


    /**
    * This method is used to return a set of WorkspaceInfo which contains the
    * information about the workspaces and it's corresponding entity which can be
    * derived for an event.
    */
    @Override
    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
        ICXLog cxLog = CXLog.fenter("derive.NFT_IbpwdcngEvent");
        Set<WorkspaceInfo> wsInfoSet = new HashSet<>();

        CxKeyHelper h = Hfdb.getCxKeyHelper();

        if (!this.accountId.equals("XXXXXX") && !this.accountId.equals("NA")) {
            if (!StringUtils.isNullOrEmpty(accountId)) {
                String accountKey = h.getCxKeyGivenHostKey(WorkspaceName.ACCOUNT, getHostId(), this.accountId);
                wsInfoSet.add(new WorkspaceInfo("Account", accountKey));
            }
        }

        if (!this.custId.equals("XXXXXX") && !this.custId.equals("NA")) {
            if (!StringUtils.isNullOrEmpty(custId)) {
                String customerKey = h.getCxKeyGivenHostKey(WorkspaceName.CUSTOMER, getHostId(), this.custId);
                wsInfoSet.add(new WorkspaceInfo("Customer", customerKey));
            }
        }

        cxLog.fexit();
        return wsInfoSet;
    }


    /**
    * This method is used to return a json to be shown as evidence on CMS (48.F+ release).
    */
    @JsonIgnore
    @Override
    public CxJson getEvidenceAsJson() {
        CxJson json = new CxJson();
        json.put("host_id", getHostId());
        return json;
    }

    /**
    * This method is used to return a json to be used in Fraud Resolution.
    * The fields present in this json are the fields which were marked with 'fr:true' in
    * the event spec along with some mandatory fields like : event_id, event_name, event_type, event_sub_type, sys_time.
    */
    @JsonIgnore
    @Override
    public CxJson getFRAsJson() {
        CxJson json = new CxJson();
        json.put("event_id", getEventId());
        json.put("event_name", "NFT_Ibpwdcng");
        json.put("event_type", "NFT");
        json.put("event_sub_type", "Ibpwdcng");
        if(getEventTS() != null) json.put("sys_time", getEventTS().getTime());
        if (!StringUtils.isNullOrEmpty(getCustId())) {
            json.put("account_id", getAccountId());
        }
        else
            json.put("account_id", "XXXXXX");

        if (!StringUtils.isNullOrEmpty(getCustId())) {
            json.put("cust_id", getCustId());
        }
        else
            json.put("cust_id", "XXXXXX");
        return json;
    }


    /**
    * This method is supposed to return a proper user understandable
    * message to be shown on the UI for end user.
    * For ex: for FT_AccountTxnEvent the message can be -> User has debited Rs 10000/- from
    * his account on 10th Feb, 2017 at 01:00 pm
    */
    @JsonIgnore
    @Override
    public String getEvidenceMessage(String factname) {
        String defaultMessage = ""; //TODO to be implemented by the developer.
        String message = EventHelper.evidenceMessage(this, factname);
        if(message != null && !"".equals(message)) return message;
        return defaultMessage;
    }
}