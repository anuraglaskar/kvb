cxps.events.event.ft-fundstransfer{
table-name : EVENT_FT_FUNDSTRANSFER
event-mnemonic : FUND
        workspaces : {
        ACCOUNT : "account-id"
        CUSTOMER : "cust-id"
        NONCUSTOMER : "payee-acct-id + payee-ifsc-code, payer-mob-no"
}
event-attributes : {

channel:{ type:"string:20"}
aadhar-no:{ type:"string:20"}
account-id:{ type:"string:20", fr : true}
cust-id:{ type:"string:20", fr : true}
corporate-id:{ type:"string:20"}
mobile-no:{ type:"string:20"}
user-id:{ type:"string:20"}
acct-name:{ type:"string:50"}
tran-type:{ type:"string:20"}
network-type:{ type:"string:20"}
auth-type:{ type:"string:20"}
tran-id:{ type:"string:20"}
tran-amt:{ type:"number:20,3"}
tran-crncy-code:{ type:"string:20"}
tran-rmks:{ type:"string:50"}
ip-address:{ type:"string:20"}
cust-card-id:{ type:"string:20"}
terminal-id:{ type:"string:20"}
device-id:{ type:"string:20"}
branch-id:{ type:"string:20"}
country-code:{ type:"string:20", derivation :"""cxps.events.CustomDerivator.getCountryCode(this)"""}
payee-id:{ type:"string:20"}
payee-acct-id:{ type:"string:20", fr : true}
payee-nick-name:{ type:"string:20"}
payee-name:{ type:"string:50"}
payee-ifsc-code:{ type:"string:20", fr : true}
payee-mob-no:{ type:"string:20"}
payee-code:{ type:"string:20"}
payer-id:{ type:"string:20"}
payer-acct-id:{ type:"string:20"}
payer-nick-name:{ type:"string:20"}
payer-name:{ type:"string:50"}
payer-ifsc-code:{ type:"string:20"}
payer-mob-no:{ type:"string:20", fr : true}
payer-code:{ type:"string:20"}
sender-vpa:{ type:"string:20"}
mcc-id:{ type:"string:20"}
city-code:{ type:"string:20"}
agent-code:{ type:"string:20"}
account-type:{ type:"string:20"}
tran-category:{ type:"string:20"}
tran-sub-category:{ type:"string:20"}
part-tran-type:{ type:"string:20"}
receiver-vpa:{ type:"string:20"}
kvb-customer:{ type:"string:2"}
avl-bal:{ type:"number:20,3" , derivation :"""cxps.events.CustomDerivator.getAvlBal(this)"""}
last-cust-induced-dt:{ type:timestamp , derivation :"""cxps.events.CustomDerivator.getLastIndDate(this)"""}
is-pool-acct:{ type:"string:2" , derivation :"""cxps.events.CustomDerivator.getIsPoolAcct(this)"""}
is-rec-pool-acct:{ type:"string:2" , derivation :"""cxps.events.CustomDerivator.getIsRecPoolAcct(this)"""}
cust-name:{ type:"string:50" , derivation :"""cxps.events.CustomDerivator.getCustName(this)"""}
cust-mobile:{ type:"string:20" , derivation :"""cxps.events.CustomDerivator.getCustMobile(this)"""}
succ-fail-flg:{type:"string:20"}
}
}
