// -- ASSISTED CODE --
package cxps.events;

import java.util.Set;
import java.util.HashSet;
import java.util.Date;
import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonIgnore;

import clari5.platform.aef.AEF;
import clari5.platform.aef.annotations.Field;
import clari5.platform.aef.annotations.Table;
import clari5.platform.aef.features.ITable;
import clari5.platform.util.CxJson;
import clari5.adb.IArchivable;
import clari5.platform.dbcon.CxConnection;
import clari5.platform.rdbms.RDBMSSession;
import clari5.platform.logger.CXLog;
import clari5.platform.logger.ICXLog;
import clari5.hfdb.CxKeyHelper;
import clari5.hfdb.Hfdb;
import clari5.hfdb.WorkspaceName;

import com.mysql.jdbc.StringUtils;
import cxps.apex.noesis.WorkspaceInfo;
import cxps.noesis.core.Event;
import cxps.noesis.core.EventHelper;


@Table(Name="EVENT_NFT_DBLOGIN", Schema="rice")
public class NFT_DbloginEvent extends Event implements IArchivable {

    @Field(size = 100, key=true) public String eventId;
    @Field                       public Timestamp eventDate;
    @Field                       public Boolean isPostTransaction;
       @Field(size=20) public String userType;
       @Field(size=20) public String corporateId;
       @Field(size=20) public String countryCode;
       @Field(size=20) public String accountId;
       @Field(size=20) public String userId;
       @Field public java.sql.Timestamp lastLogints;
       @Field(size=20) public String channel;
       @Field(size=50) public String errorDesc;
       @Field(size=2) public String succFailFlg;
       @Field(size=50) public String custName;
       @Field(size=20) public String custId;
       @Field(size=20) public String ipCountry;
       @Field(size=20) public String errorCode;
       @Field(size=20) public String deviceId;
       @Field(size=20) public String mfaType;
       @Field public java.sql.Timestamp logints;
       @Field(size=20) public String emailId;
       @Field(size=20) public String lastLoginIp;
       @Field(size=20) public String addrNetwork;
       @Field(size=20) public String custMobile;
       @Field(size=20) public String deviceType;
       @Field(size=20) public String mobileNo;
       @Field(size=20) public String isPoolAcct;


    @JsonIgnore
    public ITable<NFT_DbloginEvent> t = AEF.getITable(this);

	public NFT_DbloginEvent(){}

    public NFT_DbloginEvent(CxConnection con, Date from, Date to){
      this.t.setTblName(makeArchivable(this.t.getTableName(), con, from, to));
    }

    /**
    * This method is used to populate the event object using the data
    * coming in message body of event json from external system.
    */
    @Override
    public void from(CxJson json) {
        super.from(json);
        setEventType("NFT");
        setEventSubType("Dblogin");
        setEventName(getEventType() + "_" + getEventSubType());
        setEventId((!isPostTransaction() ? "RDA_" : "")+ json.getString("event_id"));

        if(getEventTS() != null)
            setEventDate(new Timestamp(getEventTS().getTime()));
        else
            setEventDate(new Timestamp(System.currentTimeMillis()));

            setUserType(json.getString("user_type"));
            setCorporateId(json.getString("corporate_id"));
            setAccountId(json.getString("account_id"));
            setUserId(json.getString("user_id"));
            setLastLogints(EventHelper.toTimestamp(json.getString("last_logints")));
            setChannel(json.getString("channel"));
            setErrorDesc(json.getString("error_desc"));
            setSuccFailFlg(json.getString("succ_fail_flg"));
            setCustId(json.getString("cust_id"));
            setIpCountry(json.getString("ip_country"));
            setErrorCode(json.getString("error_code"));
            setDeviceId(json.getString("device_id"));
            setMfaType(json.getString("mfa_type"));
            setLogints(EventHelper.toTimestamp(json.getString("logints")));
            setEmailId(json.getString("email_id"));
            setLastLoginIp(json.getString("last_login_ip"));
            setAddrNetwork(json.getString("addr_network"));
            setDeviceType(json.getString("device_type"));
            setMobileNo(json.getString("mobile_no"));

        setDerivedValues();

    }


    private void setDerivedValues() {
        setCountryCode(cxps.events.CustomDerivator.getCountryCode(this));setCustName(cxps.events.CustomDerivator.getCustName(this));setCustMobile(cxps.events.CustomDerivator.getCustMobile(this));setIsPoolAcct(cxps.events.CustomDerivator.getIsPoolAcct(this));
    }


    /* Getters */
    @Override
    public String getMnemonic() { return "LN"; }

    public String getEventId() { return this.eventId;}
    public Timestamp getEventDate() { return this.eventDate; }
    public String getUserType(){ return userType; }

    public String getCorporateId(){ return corporateId; }

    public String getAccountId(){ return accountId; }

    public String getUserId(){ return userId; }

    public java.sql.Timestamp getLastLogints(){ return lastLogints; }

    public String getChannel(){ return channel; }

    public String getErrorDesc(){ return errorDesc; }

    public String getSuccFailFlg(){ return succFailFlg; }

    public String getCustId(){ return custId; }

    public String getIpCountry(){ return ipCountry; }

    public String getErrorCode(){ return errorCode; }

    public String getDeviceId(){ return deviceId; }

    public String getMfaType(){ return mfaType; }

    public java.sql.Timestamp getLogints(){ return logints; }

    public String getEmailId(){ return emailId; }

    public String getLastLoginIp(){ return lastLoginIp; }

    public String getAddrNetwork(){ return addrNetwork; }

    public String getDeviceType(){ return deviceType; }

    public String getMobileNo(){ return mobileNo; }
    public String getCountryCode(){ return countryCode; }

    public String getCustName(){ return custName; }

    public String getCustMobile(){ return custMobile; }

    public String getIsPoolAcct(){ return isPoolAcct; }

    /* Setters */
    public void setEventId(String val) { this.eventId = val; }
    public void setEventDate(Timestamp val) { this.eventDate = val; }
    public void setUserType(String val){ this.userType = val; }
    public void setCorporateId(String val){ this.corporateId = val; }
    public void setAccountId(String val){ this.accountId = val; }
    public void setUserId(String val){ this.userId = val; }
    public void setLastLogints(java.sql.Timestamp val){ this.lastLogints = val; }
    public void setChannel(String val){ this.channel = val; }
    public void setErrorDesc(String val){ this.errorDesc = val; }
    public void setSuccFailFlg(String val){ this.succFailFlg = val; }
    public void setCustId(String val){ this.custId = val; }
    public void setIpCountry(String val){ this.ipCountry = val; }
    public void setErrorCode(String val){ this.errorCode = val; }
    public void setDeviceId(String val){ this.deviceId = val; }
    public void setMfaType(String val){ this.mfaType = val; }
    public void setLogints(java.sql.Timestamp val){ this.logints = val; }
    public void setEmailId(String val){ this.emailId = val; }
    public void setLastLoginIp(String val){ this.lastLoginIp = val; }
    public void setAddrNetwork(String val){ this.addrNetwork = val; }
    public void setDeviceType(String val){ this.deviceType = val; }
    public void setMobileNo(String val){ this.mobileNo = val; }
    public void setCountryCode(String val){ this.countryCode = val; }
    public void setCustName(String val){ this.custName = val; }
    public void setCustMobile(String val){ this.custMobile = val; }
    public void setIsPoolAcct(String val){ this.isPoolAcct = val; }

    /* Custom Getters*/


    /**
    * This method is used to return a set of WorkspaceInfo which contains the
    * information about the workspaces and it's corresponding entity which can be
    * derived for an event.
    */
    @Override
    public Set<WorkspaceInfo> getWorkspaceInfoSet(RDBMSSession session) {
        ICXLog cxLog = CXLog.fenter("derive.NFT_DbloginEvent");
        Set<WorkspaceInfo> wsInfoSet = new HashSet<>();

        CxKeyHelper h = Hfdb.getCxKeyHelper();

        if (!this.accountId.equals("XXXXXX") && !this.accountId.equals("NA")) {
            if (!StringUtils.isNullOrEmpty(accountId)) {
                String accountKey = h.getCxKeyGivenHostKey(WorkspaceName.ACCOUNT, getHostId(), this.accountId);
                wsInfoSet.add(new WorkspaceInfo("Account", accountKey));
            }
        }
        if (!this.addrNetwork.equals("XXXXXX") && !this.addrNetwork.equals("NA")) {
            if (!StringUtils.isNullOrEmpty(addrNetwork)) {
                String noncustomerKey = h.getCxKeyGivenHostKey(WorkspaceName.NONCUSTOMER, getHostId(), this.addrNetwork);
                wsInfoSet.add(new WorkspaceInfo("Noncustomer", noncustomerKey));
            }
        }
        if (!this.custId.equals("XXXXXX") && !this.custId.equals("NA")) {
            if (!StringUtils.isNullOrEmpty(custId)) {
                String customerKey = h.getCxKeyGivenHostKey(WorkspaceName.CUSTOMER, getHostId(), this.custId);
                wsInfoSet.add(new WorkspaceInfo("Customer", customerKey));
            }
        }

        cxLog.fexit();
        return wsInfoSet;
    }


    /**
    * This method is used to return a json to be shown as evidence on CMS (48.F+ release).
    */
    @JsonIgnore
    @Override
    public CxJson getEvidenceAsJson() {
        CxJson json = new CxJson();
        json.put("host_id", getHostId());
        return json;
    }

    /**
    * This method is used to return a json to be used in Fraud Resolution.
    * The fields present in this json are the fields which were marked with 'fr:true' in
    * the event spec along with some mandatory fields like : event_id, event_name, event_type, event_sub_type, sys_time.
    */
    @JsonIgnore
    @Override
    public CxJson getFRAsJson() {
        CxJson json = new CxJson();
        json.put("event_id", getEventId());
        json.put("event_name", "NFT_Dblogin");
        json.put("event_type", "NFT");
        json.put("event_sub_type", "Dblogin");
        if(getEventTS() != null) json.put("sys_time", getEventTS().getTime());
        if (!StringUtils.isNullOrEmpty(getAccountId())) {
            json.put("account_id", getAccountId());
        }
        else
            json.put("account_id", "XXXXXX");

        if (!StringUtils.isNullOrEmpty(getCustId())) {
            json.put("cust_id", getCustId());
        }
        else
            json.put("cust_id", "XXXXXX");

        if (!StringUtils.isNullOrEmpty(getAddrNetwork())) {
            json.put("addr_network", getAddrNetwork());
        }
        else
            json.put("addR_network", "XXXXXX");
        return json;
    }


    /**
    * This method is supposed to return a proper user understandable
    * message to be shown on the UI for end user.
    * For ex: for FT_AccountTxnEvent the message can be -> User has debited Rs 10000/- from
    * his account on 10th Feb, 2017 at 01:00 pm
    */
    @JsonIgnore
    @Override
    public String getEvidenceMessage(String factname) {
        String defaultMessage = ""; //TODO to be implemented by the developer.
        String message = EventHelper.evidenceMessage(this, factname);
        if(message != null && !"".equals(message)) return message;
        return defaultMessage;
    }
}