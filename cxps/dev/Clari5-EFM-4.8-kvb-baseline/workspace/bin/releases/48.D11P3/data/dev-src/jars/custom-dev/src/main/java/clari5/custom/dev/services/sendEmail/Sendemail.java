package clari5.custom.dev.services.sendEmail;

import clari5.custom.dev.DbUtil.Dbfetch;
import clari5.custom.dev.services.SmsemailutilityException;
import clari5.custom.dev.services.StrSubstitutor;
import clari5.tools.util.Hocon;
import clari5.platform.logger.CxpsLogger;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

public class Sendemail extends HttpServlet {
	private static CxpsLogger logger = CxpsLogger.getLogger(Sendemail.class);
	private static Hocon hocon = null;
	private static String userName = "";
	private static String password = "";
	private static String sender = "";
	private static String emailTemplate = "";
	static {
		hocon = new Hocon();
		hocon.loadFromContext("email-details.conf");
		userName = hocon.getString("email.username");
		password = hocon.getString("email.password");
		sender = hocon.getString("email.sender");
		emailTemplate = hocon.getString("email.template");

	}

	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException {
		String issue_key = req.getParameter("issuekey");
		String callBack = req.getParameter("callback");
		String response = "";
		String cust_id = "";
		String custEmailAddress = "";
		String custAccountId = "";
		String custAccountIdFull = "";
		String eventDate = "";

		PrintWriter out=null;
		String resp=null;
		out = res.getWriter();
		res.setContentType("application/javascript");
		logger.info("Preparing Email");
		/**
		 * Getting the transaction details from db
		 */
		Dbfetch db = new Dbfetch();
		List<String> tranDetails = db.getTranDetails(issue_key);
		if(tranDetails.size() != 0) {
			if((tranDetails.get(0) == null || tranDetails.get(0).isEmpty()) || (tranDetails.get(1) == null || tranDetails.get(1).isEmpty()) || (tranDetails.get(2) == null || tranDetails.get(2).isEmpty())) {
				try{
					throw new SmsemailutilityException("Insufficient Data. Customer ID or Account ID or Transaction Date or Email missing");
				}catch (Exception e) {
					e.printStackTrace();
				}
			} else {
				cust_id = tranDetails.get(0);
				custEmailAddress = db.getCustEmail(cust_id);
				custAccountIdFull = tranDetails.get(1);
				if (custAccountIdFull.length() > 4) {
					custAccountId = custAccountIdFull.substring(custAccountIdFull.length() - 4);
				} else {
					custAccountId = custAccountIdFull;
				}
				eventDate = tranDetails.get(2);
			}
		} else {
			//System.out.println("No Transaction Details found against the issue-key "+issue_key);
			logger.info("No Transaction Details found against the issue-key "+issue_key);
			response = "({\"Response\":\"Failed to send email request\"})";
			resp = callBack+response;
			out.print(resp);
			out.flush();
			try {
				throw new SmsemailutilityException("Insufficient data");
			} catch (SmsemailutilityException e) {
				e.printStackTrace();
			}
			return;
		}

		//Substitute transaction details in email template
		Map<String, String> emailPlaceHolders = new HashMap<String, String>();
		emailPlaceHolders.put("account_id", custAccountId);
		emailPlaceHolders.put("tran_date", eventDate);
		StrSubstitutor ssub = new StrSubstitutor(emailPlaceHolders);
		String emailContent = ssub.replace(emailTemplate);
		/*SMTP configuartion */
		Properties prop = new Properties();
		prop.put("mail.smtp.host", hocon.getString("mail.smtp.host"));
		prop.put("mail.smtp.port", hocon.getString("mail.smtp.port"));


		if(hocon.getString("mail.smtp.auth.type").equals("TLS")) {
			prop.put("mail.smtp.auth", "true");
			prop.put("mail.smtp.starttls.enable", "true");
		}
		else if(hocon.getString("mail.smtp.auth.type").equals("SSL")) {
			prop.put("mail.smtp.auth", "true");
			prop.put("mail.smtp.socketFactory.port", hocon.getString("mail.smtp.port"));
			prop.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		}
		else if(hocon.getString("mail.smtp.auth.type").equals("NO"))
			prop.put("mail.smtp.auth", "false");
		else
			prop.put("mail.smtp.auth", "true");

		Authenticator auth = new Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(userName, password);
			}
		};
		Session session = Session.getDefaultInstance(prop, auth);
		try {
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(sender));
			message.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse(custEmailAddress)
			);
			message.setSubject(hocon.getString("email.subject"));
			/*message.setText(emailContent);*/
			message.setContent(emailContent, "text/html");
			try {
				Transport.send(message);
				//System.out.println("Mail Sent to the relevant customer");
				logger.info("Request Sent to the SMTP Server");
				/*
				UPDATING SMSEMAILDETAILS TABLE
				 */
				db.updateSmsEmailDetailsTable("EMAIL", custEmailAddress, "", cust_id, "Email Request Sent");

				/*
				CREATING RESPONSE
				 */
				response = "({\"Response\":\"Successful\"})";
				resp = callBack+response;
				out.print(resp);
				out.flush();
			} catch (SendFailedException e) {
				//System.out.println("Couldn't send mail !!Please go through the logs for further details");
				logger.error("Request to SMTP server failed. Check credentials or network connectivity ");
				/*
				UPDATING SMSEMAILDETAILS TABLE
				 */
				db.updateSmsEmailDetailsTable("EMAIL", custEmailAddress, "", cust_id, "Failed to send email request. Check Logs");

				/*
				CREATING RESPONSE
				 */
				response = "({\"Response\":\"Failed to send email\"})";
				resp = callBack+response;
				out.print(resp);
				out.flush();
				e.printStackTrace();
			}
		}
		catch (Exception e) {
			/*
				UPDATING SMSEMAILDETAILS TABLE
			*/
			db.updateSmsEmailDetailsTable("EMAIL", custEmailAddress, "", cust_id, "Failed to send email. Check Logs");

			/*
				CREATING RESPONSE
			*/
			response = "({\"Response\":\"Failed to send email\"})";
			resp = callBack+response;
			out.print(resp);
			out.flush();

			e.printStackTrace();
		}
	}
}
