// -- ASSISTED CODE --
package cxps.events;

import clari5.platform.dbcon.CxConnection;
import clari5.platform.dbcon.DbTypeEnum;
import clari5.trace.ConnectionWrapper;
import clari5.trace.ConnectionWrapperException;
import clari5.trace.mappers.EventMapper;
import cxps.apex.shared.IWSEvent;
import cxps.noesis.core.EventHelper;

import com.google.protobuf.ServiceException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;


import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

public class FT_FundstransferEventMapper extends EventMapper<FT_FundstransferEvent> {

public FT_FundstransferEventMapper () {}

@Override
public void save(ConnectionWrapper connectionWrapper, List<FT_FundstransferEvent> list) throws ConnectionWrapperException {
  switch (connectionWrapper.getType()){
    case HBASE:
        saveInHbase(connectionWrapper.getHbaseConnection(),list);
        break;
    case RDBMS:
        saveInRDBMS(connectionWrapper.getCxConnection(),list);
        break;
    }
}


@Override
public List<IWSEvent> getAllEvents(ConnectionWrapper connectionWrapper, Set<String> set) throws ConnectionWrapperException {
   List<IWSEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      allevents = getAllEventsFromHbase(connectionWrapper.getHbaseConnection(),set);
      return allevents;
    case RDBMS:
      allevents = getAllEventsFromRDBMS(connectionWrapper.getCxConnection(),set);
      return allevents;
   }
   return null;
}

@Override
public List<FT_FundstransferEvent> getAllEventsFromArchive(ConnectionWrapper connectionWrapper, Set<String> set, Date from, Date to) throws ConnectionWrapperException {
   List<FT_FundstransferEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      return new ArrayList<>();
    case RDBMS:
      allevents = getArchivedEventsFromRDBMS(connectionWrapper.getCxConnection(),set, from, to);
      return allevents;
   }
   return new ArrayList<>();
}


public void saveInRDBMS(CxConnection con,  List<FT_FundstransferEvent> eventList) throws ConnectionWrapperException {
 if(eventList == null) return;
 if(eventList.isEmpty()) return;
 if(!FT_FundstransferEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

 try (PreparedStatement preparedStatement = con.prepareStatement(this.saveQuery(con.getDbType()))) {
        for (FT_FundstransferEvent obj : eventList) {
            int i = 1;
            preparedStatement.setString(i++,obj.getEventId());
            preparedStatement.setTimestamp(i++,obj.getEventDate());
            preparedStatement.setString(i++, obj.isPostTransaction() ? "Y" : "N");
            preparedStatement.setString(i++, obj.getPayeeCode());
            preparedStatement.setString(i++, obj.getChannel());
            preparedStatement.setString(i++, obj.getPayerAcctId());
            preparedStatement.setString(i++, obj.getPayerName());
            preparedStatement.setString(i++, obj.getAadharNo());
            preparedStatement.setString(i++, obj.getDeviceId());
            preparedStatement.setString(i++, obj.getPayeeAcctId());
            preparedStatement.setString(i++, obj.getPayerIfscCode());
            preparedStatement.setString(i++, obj.getAgentCode());
            preparedStatement.setString(i++, obj.getTranSubCategory());
            preparedStatement.setString(i++, obj.getAccountType());
            preparedStatement.setString(i++, obj.getTranId());
            preparedStatement.setString(i++, obj.getIsPoolAcct());
            preparedStatement.setDouble(i++, obj.getTranAmt());
            preparedStatement.setString(i++, obj.getAccountId());
            preparedStatement.setString(i++, obj.getCustName());
            preparedStatement.setString(i++, obj.getPayeeName());
            preparedStatement.setString(i++, obj.getBranchId());
            preparedStatement.setString(i++, obj.getSenderVpa());
            preparedStatement.setString(i++, obj.getTranCrncyCode());
            preparedStatement.setString(i++, obj.getTranRmks());
            preparedStatement.setString(i++, obj.getTerminalId());
            preparedStatement.setString(i++, obj.getAcctName());
            preparedStatement.setString(i++, obj.getCustCardId());
            preparedStatement.setString(i++, obj.getPayeeIfscCode());
            preparedStatement.setDouble(i++, obj.getAvlBal());
            preparedStatement.setString(i++, obj.getPayeeId());
            preparedStatement.setString(i++, obj.getPayerMobNo());
            preparedStatement.setString(i++, obj.getIsRecPoolAcct());
            preparedStatement.setString(i++, obj.getCorporateId());
            preparedStatement.setString(i++, obj.getCountryCode());
            preparedStatement.setString(i++, obj.getCityCode());
            preparedStatement.setString(i++, obj.getSuccFailFlg());
            preparedStatement.setString(i++, obj.getPayeeNickName());
            preparedStatement.setTimestamp(i++, obj.getLastCustInducedDt());
            preparedStatement.setString(i++, obj.getCustMobile());
            preparedStatement.setString(i++, obj.getTranCategory());
            preparedStatement.setString(i++, obj.getPayerNickName());
            preparedStatement.setString(i++, obj.getPayerCode());
            preparedStatement.setString(i++, obj.getReceiverVpa());
            preparedStatement.setString(i++, obj.getKvbCustomer());
            preparedStatement.setString(i++, obj.getUserId());
            preparedStatement.setString(i++, obj.getCustId());
            preparedStatement.setString(i++, obj.getAuthType());
            preparedStatement.setString(i++, obj.getPayerId());
            preparedStatement.setString(i++, obj.getNetworkType());
            preparedStatement.setString(i++, obj.getIpAddress());
            preparedStatement.setString(i++, obj.getTranType());
            preparedStatement.setString(i++, obj.getMobileNo());
            preparedStatement.setString(i++, obj.getMccId());
            preparedStatement.setString(i++, obj.getPayeeMobNo());
            preparedStatement.setString(i++, obj.getPartTranType());

            preparedStatement.addBatch();
        }
        preparedStatement.executeBatch();
    }catch (Exception e) {
        System.out.println("(RDBMS) Exception in saving data in [EVENT_FT_FUNDSTRANSFER]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    }
}

public void saveInHbase(Connection connection, List<FT_FundstransferEvent> eventList) throws ConnectionWrapperException {
    if(eventList == null) return;
    if(eventList.isEmpty()) return;
    if(!FT_FundstransferEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

    Table table = null;
    List <Put> putList = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_FT_FUNDSTRANSFER"));
        putList = new ArrayList<>();

        for (FT_FundstransferEvent obj : eventList){

            Put p = new Put(Bytes.toBytes(obj.getEventId()));
            Timestamp ts = obj.getEventDate();
            p = this.insert(p,"EVENT_ID",obj.getEventId());
            p = this.insert(p,"EVENT_DATE", ts == null ? null : String.valueOf(ts));
            p = this.insert(p,"IS_POST_TRANSACTION",obj.isPostTransaction() ? "Y" : "N");
            p = this.insert(p, "PAYEE_CODE",  obj.getPayeeCode());
            p = this.insert(p, "CHANNEL",  obj.getChannel());
            p = this.insert(p, "PAYER_ACCT_ID",  obj.getPayerAcctId());
            p = this.insert(p, "PAYER_NAME",  obj.getPayerName());
            p = this.insert(p, "AADHAR_NO",  obj.getAadharNo());
            p = this.insert(p, "DEVICE_ID",  obj.getDeviceId());
            p = this.insert(p, "PAYEE_ACCT_ID",  obj.getPayeeAcctId());
            p = this.insert(p, "PAYER_IFSC_CODE",  obj.getPayerIfscCode());
            p = this.insert(p, "AGENT_CODE",  obj.getAgentCode());
            p = this.insert(p, "TRAN_SUB_CATEGORY",  obj.getTranSubCategory());
            p = this.insert(p, "ACCOUNT_TYPE",  obj.getAccountType());
            p = this.insert(p, "TRAN_ID",  obj.getTranId());
            p = this.insert(p, "IS_POOL_ACCT",  obj.getIsPoolAcct());
            p = this.insert(p, "TRAN_AMT", String.valueOf(obj.getTranAmt()));
            p = this.insert(p, "ACCOUNT_ID",  obj.getAccountId());
            p = this.insert(p, "CUST_NAME",  obj.getCustName());
            p = this.insert(p, "PAYEE_NAME",  obj.getPayeeName());
            p = this.insert(p, "BRANCH_ID",  obj.getBranchId());
            p = this.insert(p, "SENDER_VPA",  obj.getSenderVpa());
            p = this.insert(p, "TRAN_CRNCY_CODE",  obj.getTranCrncyCode());
            p = this.insert(p, "TRAN_RMKS",  obj.getTranRmks());
            p = this.insert(p, "TERMINAL_ID",  obj.getTerminalId());
            p = this.insert(p, "ACCT_NAME",  obj.getAcctName());
            p = this.insert(p, "CUST_CARD_ID",  obj.getCustCardId());
            p = this.insert(p, "PAYEE_IFSC_CODE",  obj.getPayeeIfscCode());
            p = this.insert(p, "AVL_BAL", String.valueOf(obj.getAvlBal()));
            p = this.insert(p, "PAYEE_ID",  obj.getPayeeId());
            p = this.insert(p, "PAYER_MOB_NO",  obj.getPayerMobNo());
            p = this.insert(p, "IS_REC_POOL_ACCT",  obj.getIsRecPoolAcct());
            p = this.insert(p, "CORPORATE_ID",  obj.getCorporateId());
            p = this.insert(p, "COUNTRY_CODE",  obj.getCountryCode());
            p = this.insert(p, "CITY_CODE",  obj.getCityCode());
            p = this.insert(p, "SUCC_FAIL_FLG",  obj.getSuccFailFlg());
            p = this.insert(p, "PAYEE_NICK_NAME",  obj.getPayeeNickName());
            p = this.insert(p, "LAST_CUST_INDUCED_DT", String.valueOf(obj.getLastCustInducedDt()));
            p = this.insert(p, "CUST_MOBILE",  obj.getCustMobile());
            p = this.insert(p, "TRAN_CATEGORY",  obj.getTranCategory());
            p = this.insert(p, "PAYER_NICK_NAME",  obj.getPayerNickName());
            p = this.insert(p, "PAYER_CODE",  obj.getPayerCode());
            p = this.insert(p, "RECEIVER_VPA",  obj.getReceiverVpa());
            p = this.insert(p, "KVB_CUSTOMER",  obj.getKvbCustomer());
            p = this.insert(p, "USER_ID",  obj.getUserId());
            p = this.insert(p, "CUST_ID",  obj.getCustId());
            p = this.insert(p, "AUTH_TYPE",  obj.getAuthType());
            p = this.insert(p, "PAYER_ID",  obj.getPayerId());
            p = this.insert(p, "NETWORK_TYPE",  obj.getNetworkType());
            p = this.insert(p, "IP_ADDRESS",  obj.getIpAddress());
            p = this.insert(p, "TRAN_TYPE",  obj.getTranType());
            p = this.insert(p, "MOBILE_NO",  obj.getMobileNo());
            p = this.insert(p, "MCC_ID",  obj.getMccId());
            p = this.insert(p, "PAYEE_MOB_NO",  obj.getPayeeMobNo());
            p = this.insert(p, "PART_TRAN_TYPE",  obj.getPartTranType());
            putList.add(p);
        }
        table.put(putList);

    } catch (IOException e) {
        Admin admin = null;
        try {
            //if table does not exists, create table and insert again.
            admin = connection.getAdmin();
            HTableDescriptor hTableDescriptor = new HTableDescriptor(TableName.valueOf("EVENT_FT_FUNDSTRANSFER"));
            hTableDescriptor.addFamily(new HColumnDescriptor("EVENT"));
            admin.createTable(hTableDescriptor);
            table.put(putList);
        } catch (Exception e1) {
            System.out.println("(HBASE) Exception in saving data in [EVENT_FT_FUNDSTRANSFER]: "+e.getMessage());
            throw new ConnectionWrapperException(e1.getMessage(), e1.getCause());
        } finally {
            try {
                if (admin != null)
                    admin.close();
                if (table != null)
                    table.close();
            } catch (IOException e1) {
                System.out.println("(HBASE) Exception for [EVENT_FT_FUNDSTRANSFER]: "+e.getMessage());
                throw new ConnectionWrapperException(e.getMessage(), e.getCause());
            }
        }
    }

}


public List<IWSEvent> getAllEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds) throws ConnectionWrapperException {
 List<IWSEvent> events = new ArrayList<>();
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");
 try{
 cxConnection.query(this.fetchQuery(cxConnection.getDbType()) + " where event_id in ("+ sb.toString() + ")", rs -> {
 while (rs.next()) {
    FT_FundstransferEvent obj = new FT_FundstransferEvent();
    obj.setEventId(rs.getString("EVENT_ID"));
    obj.setEventDate(rs.getTimestamp("EVENT_DATE"));
    obj.setEventTS(obj.getEventDate());
    obj.setPostTransaction(rs.getString("IS_POST_TRANSACTION").equals("Y"));

    obj.setPayeeCode(rs.getString("PAYEE_CODE"));
    obj.setChannel(rs.getString("CHANNEL"));
    obj.setPayerAcctId(rs.getString("PAYER_ACCT_ID"));
    obj.setPayerName(rs.getString("PAYER_NAME"));
    obj.setAadharNo(rs.getString("AADHAR_NO"));
    obj.setDeviceId(rs.getString("DEVICE_ID"));
    obj.setPayeeAcctId(rs.getString("PAYEE_ACCT_ID"));
    obj.setPayerIfscCode(rs.getString("PAYER_IFSC_CODE"));
    obj.setAgentCode(rs.getString("AGENT_CODE"));
    obj.setTranSubCategory(rs.getString("TRAN_SUB_CATEGORY"));
    obj.setAccountType(rs.getString("ACCOUNT_TYPE"));
    obj.setTranId(rs.getString("TRAN_ID"));
    obj.setIsPoolAcct(rs.getString("IS_POOL_ACCT"));
    obj.setTranAmt(rs.getDouble("TRAN_AMT"));
    obj.setAccountId(rs.getString("ACCOUNT_ID"));
    obj.setCustName(rs.getString("CUST_NAME"));
    obj.setPayeeName(rs.getString("PAYEE_NAME"));
    obj.setBranchId(rs.getString("BRANCH_ID"));
    obj.setSenderVpa(rs.getString("SENDER_VPA"));
    obj.setTranCrncyCode(rs.getString("TRAN_CRNCY_CODE"));
    obj.setTranRmks(rs.getString("TRAN_RMKS"));
    obj.setTerminalId(rs.getString("TERMINAL_ID"));
    obj.setAcctName(rs.getString("ACCT_NAME"));
    obj.setCustCardId(rs.getString("CUST_CARD_ID"));
    obj.setPayeeIfscCode(rs.getString("PAYEE_IFSC_CODE"));
    obj.setAvlBal(rs.getDouble("AVL_BAL"));
    obj.setPayeeId(rs.getString("PAYEE_ID"));
    obj.setPayerMobNo(rs.getString("PAYER_MOB_NO"));
    obj.setIsRecPoolAcct(rs.getString("IS_REC_POOL_ACCT"));
    obj.setCorporateId(rs.getString("CORPORATE_ID"));
    obj.setCountryCode(rs.getString("COUNTRY_CODE"));
    obj.setCityCode(rs.getString("CITY_CODE"));
    obj.setSuccFailFlg(rs.getString("SUCC_FAIL_FLG"));
    obj.setPayeeNickName(rs.getString("PAYEE_NICK_NAME"));
    obj.setLastCustInducedDt(rs.getTimestamp("LAST_CUST_INDUCED_DT"));
    obj.setCustMobile(rs.getString("CUST_MOBILE"));
    obj.setTranCategory(rs.getString("TRAN_CATEGORY"));
    obj.setPayerNickName(rs.getString("PAYER_NICK_NAME"));
    obj.setPayerCode(rs.getString("PAYER_CODE"));
    obj.setReceiverVpa(rs.getString("RECEIVER_VPA"));
    obj.setKvbCustomer(rs.getString("KVB_CUSTOMER"));
    obj.setUserId(rs.getString("USER_ID"));
    obj.setCustId(rs.getString("CUST_ID"));
    obj.setAuthType(rs.getString("AUTH_TYPE"));
    obj.setPayerId(rs.getString("PAYER_ID"));
    obj.setNetworkType(rs.getString("NETWORK_TYPE"));
    obj.setIpAddress(rs.getString("IP_ADDRESS"));
    obj.setTranType(rs.getString("TRAN_TYPE"));
    obj.setMobileNo(rs.getString("MOBILE_NO"));
    obj.setMccId(rs.getString("MCC_ID"));
    obj.setPayeeMobNo(rs.getString("PAYEE_MOB_NO"));
    obj.setPartTranType(rs.getString("PART_TRAN_TYPE"));

    events.add(obj);
   }});
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting data from [EVENT_FT_FUNDSTRANSFER]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<FT_FundstransferEvent> getArchivedEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds, Date from, Date to) throws ConnectionWrapperException {
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");

 List<FT_FundstransferEvent> events;
 FT_FundstransferEvent obj = new FT_FundstransferEvent(cxConnection, from, to);
 try{
     events = obj.t.loadAllFilter(cxConnection," where event_id in ("+ sb.toString() + ")", null, null);
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting archived data from ["+obj.t.getTableName()+"]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<IWSEvent> getAllEventsFromHbase(Connection connection, Set<String> eventIds) throws ConnectionWrapperException {

    List<IWSEvent> events = new ArrayList<>();
    List<Get> getList = new ArrayList<>();
    Table table = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_FT_FUNDSTRANSFER"));
        for (String id : eventIds){
            Get g = new Get(Bytes.toBytes(id));
            getList.add(g);
        }
        Result[] results = table.get(getList);
        for (Result rs : results){
            FT_FundstransferEvent obj = new FT_FundstransferEvent();
            obj.setEventId(getColumnValue(rs, "EVENT_ID"));
            String ts = getColumnValue(rs, "EVENT_DATE");
            obj.setEventDate((ts == null || ts.trim().equalsIgnoreCase("")) ? null : new Timestamp(Long.parseLong(ts.trim())));
            obj.setEventTS(obj.getEventDate());
            obj.setPostTransaction(getColumnValue(rs,"IS_POST_TRANSACTION").equals("Y"));

            obj.setPayeeCode(getColumnValue(rs, "PAYEE_CODE"));
            obj.setChannel(getColumnValue(rs, "CHANNEL"));
            obj.setPayerAcctId(getColumnValue(rs, "PAYER_ACCT_ID"));
            obj.setPayerName(getColumnValue(rs, "PAYER_NAME"));
            obj.setAadharNo(getColumnValue(rs, "AADHAR_NO"));
            obj.setDeviceId(getColumnValue(rs, "DEVICE_ID"));
            obj.setPayeeAcctId(getColumnValue(rs, "PAYEE_ACCT_ID"));
            obj.setPayerIfscCode(getColumnValue(rs, "PAYER_IFSC_CODE"));
            obj.setAgentCode(getColumnValue(rs, "AGENT_CODE"));
            obj.setTranSubCategory(getColumnValue(rs, "TRAN_SUB_CATEGORY"));
            obj.setAccountType(getColumnValue(rs, "ACCOUNT_TYPE"));
            obj.setTranId(getColumnValue(rs, "TRAN_ID"));
            obj.setIsPoolAcct(getColumnValue(rs, "IS_POOL_ACCT"));
            obj.setTranAmt(EventHelper.toDouble(getColumnValue(rs, "TRAN_AMT")));
            obj.setAccountId(getColumnValue(rs, "ACCOUNT_ID"));
            obj.setCustName(getColumnValue(rs, "CUST_NAME"));
            obj.setPayeeName(getColumnValue(rs, "PAYEE_NAME"));
            obj.setBranchId(getColumnValue(rs, "BRANCH_ID"));
            obj.setSenderVpa(getColumnValue(rs, "SENDER_VPA"));
            obj.setTranCrncyCode(getColumnValue(rs, "TRAN_CRNCY_CODE"));
            obj.setTranRmks(getColumnValue(rs, "TRAN_RMKS"));
            obj.setTerminalId(getColumnValue(rs, "TERMINAL_ID"));
            obj.setAcctName(getColumnValue(rs, "ACCT_NAME"));
            obj.setCustCardId(getColumnValue(rs, "CUST_CARD_ID"));
            obj.setPayeeIfscCode(getColumnValue(rs, "PAYEE_IFSC_CODE"));
            obj.setAvlBal(EventHelper.toDouble(getColumnValue(rs, "AVL_BAL")));
            obj.setPayeeId(getColumnValue(rs, "PAYEE_ID"));
            obj.setPayerMobNo(getColumnValue(rs, "PAYER_MOB_NO"));
            obj.setIsRecPoolAcct(getColumnValue(rs, "IS_REC_POOL_ACCT"));
            obj.setCorporateId(getColumnValue(rs, "CORPORATE_ID"));
            obj.setCountryCode(getColumnValue(rs, "COUNTRY_CODE"));
            obj.setCityCode(getColumnValue(rs, "CITY_CODE"));
            obj.setSuccFailFlg(getColumnValue(rs, "SUCC_FAIL_FLG"));
            obj.setPayeeNickName(getColumnValue(rs, "PAYEE_NICK_NAME"));
            obj.setLastCustInducedDt(EventHelper.toTimestamp(getColumnValue(rs, "LAST_CUST_INDUCED_DT")));
            obj.setCustMobile(getColumnValue(rs, "CUST_MOBILE"));
            obj.setTranCategory(getColumnValue(rs, "TRAN_CATEGORY"));
            obj.setPayerNickName(getColumnValue(rs, "PAYER_NICK_NAME"));
            obj.setPayerCode(getColumnValue(rs, "PAYER_CODE"));
            obj.setReceiverVpa(getColumnValue(rs, "RECEIVER_VPA"));
            obj.setKvbCustomer(getColumnValue(rs, "KVB_CUSTOMER"));
            obj.setUserId(getColumnValue(rs, "USER_ID"));
            obj.setCustId(getColumnValue(rs, "CUST_ID"));
            obj.setAuthType(getColumnValue(rs, "AUTH_TYPE"));
            obj.setPayerId(getColumnValue(rs, "PAYER_ID"));
            obj.setNetworkType(getColumnValue(rs, "NETWORK_TYPE"));
            obj.setIpAddress(getColumnValue(rs, "IP_ADDRESS"));
            obj.setTranType(getColumnValue(rs, "TRAN_TYPE"));
            obj.setMobileNo(getColumnValue(rs, "MOBILE_NO"));
            obj.setMccId(getColumnValue(rs, "MCC_ID"));
            obj.setPayeeMobNo(getColumnValue(rs, "PAYEE_MOB_NO"));
            obj.setPartTranType(getColumnValue(rs, "PART_TRAN_TYPE"));

            events.add(obj);
        }
        return events;

    } catch (IOException e) {
        System.out.println("(HBASE) Exception in getting data from [EVENT_FT_FUNDSTRANSFER]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    } finally {
        if (table != null) try {
            table.close();
        } catch (IOException e) {
            System.out.println("(HBASE) Exception in getting data from [EVENT_FT_FUNDSTRANSFER]: "+e.getMessage());
            throw new ConnectionWrapperException(e.getMessage(), e.getCause());
        }
    }
}


private String fetchQuery(DbTypeEnum dbTypeEnum) {
 switch (dbTypeEnum) {
  case ORACLE:
      return "SELECT \"EVENT_ID\", \"EVENT_DATE\",\"IS_POST_TRANSACTION\", \"PAYEE_CODE\",\"CHANNEL\",\"PAYER_ACCT_ID\",\"PAYER_NAME\",\"AADHAR_NO\",\"DEVICE_ID\",\"PAYEE_ACCT_ID\",\"PAYER_IFSC_CODE\",\"AGENT_CODE\",\"TRAN_SUB_CATEGORY\",\"ACCOUNT_TYPE\",\"TRAN_ID\",\"IS_POOL_ACCT\",\"TRAN_AMT\",\"ACCOUNT_ID\",\"CUST_NAME\",\"PAYEE_NAME\",\"BRANCH_ID\",\"SENDER_VPA\",\"TRAN_CRNCY_CODE\",\"TRAN_RMKS\",\"TERMINAL_ID\",\"ACCT_NAME\",\"CUST_CARD_ID\",\"PAYEE_IFSC_CODE\",\"AVL_BAL\",\"PAYEE_ID\",\"PAYER_MOB_NO\",\"IS_REC_POOL_ACCT\",\"CORPORATE_ID\",\"COUNTRY_CODE\",\"CITY_CODE\",\"SUCC_FAIL_FLG\",\"PAYEE_NICK_NAME\",\"LAST_CUST_INDUCED_DT\",\"CUST_MOBILE\",\"TRAN_CATEGORY\",\"PAYER_NICK_NAME\",\"PAYER_CODE\",\"RECEIVER_VPA\",\"KVB_CUSTOMER\",\"USER_ID\",\"CUST_ID\",\"AUTH_TYPE\",\"PAYER_ID\",\"NETWORK_TYPE\",\"IP_ADDRESS\",\"TRAN_TYPE\",\"MOBILE_NO\",\"MCC_ID\",\"PAYEE_MOB_NO\",\"PART_TRAN_TYPE\"" +
              " FROM EVENT_FT_FUNDSTRANSFER";
  case SQLSERVER:
      return "SELECT [EVENT_ID], [EVENT_DATE],[IS_POST_TRANSACTION], [PAYEE_CODE],[CHANNEL],[PAYER_ACCT_ID],[PAYER_NAME],[AADHAR_NO],[DEVICE_ID],[PAYEE_ACCT_ID],[PAYER_IFSC_CODE],[AGENT_CODE],[TRAN_SUB_CATEGORY],[ACCOUNT_TYPE],[TRAN_ID],[IS_POOL_ACCT],[TRAN_AMT],[ACCOUNT_ID],[CUST_NAME],[PAYEE_NAME],[BRANCH_ID],[SENDER_VPA],[TRAN_CRNCY_CODE],[TRAN_RMKS],[TERMINAL_ID],[ACCT_NAME],[CUST_CARD_ID],[PAYEE_IFSC_CODE],[AVL_BAL],[PAYEE_ID],[PAYER_MOB_NO],[IS_REC_POOL_ACCT],[CORPORATE_ID],[COUNTRY_CODE],[CITY_CODE],[SUCC_FAIL_FLG],[PAYEE_NICK_NAME],[LAST_CUST_INDUCED_DT],[CUST_MOBILE],[TRAN_CATEGORY],[PAYER_NICK_NAME],[PAYER_CODE],[RECEIVER_VPA],[KVB_CUSTOMER],[USER_ID],[CUST_ID],[AUTH_TYPE],[PAYER_ID],[NETWORK_TYPE],[IP_ADDRESS],[TRAN_TYPE],[MOBILE_NO],[MCC_ID],[PAYEE_MOB_NO],[PART_TRAN_TYPE]" +
              " FROM EVENT_FT_FUNDSTRANSFER";
  case MYSQL:
      return "SELECT `EVENT_ID`, `EVENT_DATE`,`IS_POST_TRANSACTION`,`PAYEE_CODE`,`CHANNEL`,`PAYER_ACCT_ID`,`PAYER_NAME`,`AADHAR_NO`,`DEVICE_ID`,`PAYEE_ACCT_ID`,`PAYER_IFSC_CODE`,`AGENT_CODE`,`TRAN_SUB_CATEGORY`,`ACCOUNT_TYPE`,`TRAN_ID`,`IS_POOL_ACCT`,`TRAN_AMT`,`ACCOUNT_ID`,`CUST_NAME`,`PAYEE_NAME`,`BRANCH_ID`,`SENDER_VPA`,`TRAN_CRNCY_CODE`,`TRAN_RMKS`,`TERMINAL_ID`,`ACCT_NAME`,`CUST_CARD_ID`,`PAYEE_IFSC_CODE`,`AVL_BAL`,`PAYEE_ID`,`PAYER_MOB_NO`,`IS_REC_POOL_ACCT`,`CORPORATE_ID`,`COUNTRY_CODE`,`CITY_CODE`,`SUCC_FAIL_FLG`,`PAYEE_NICK_NAME`,`LAST_CUST_INDUCED_DT`,`CUST_MOBILE`,`TRAN_CATEGORY`,`PAYER_NICK_NAME`,`PAYER_CODE`,`RECEIVER_VPA`,`KVB_CUSTOMER`,`USER_ID`,`CUST_ID`,`AUTH_TYPE`,`PAYER_ID`,`NETWORK_TYPE`,`IP_ADDRESS`,`TRAN_TYPE`,`MOBILE_NO`,`MCC_ID`,`PAYEE_MOB_NO`,`PART_TRAN_TYPE`" +
              " FROM EVENT_FT_FUNDSTRANSFER";
 }
  return null;
}

private String saveQuery(DbTypeEnum dbType) {

 String insertQuery="INSERT INTO EVENT_FT_FUNDSTRANSFER (";
 switch(dbType){
    case ORACLE:
        insertQuery=insertQuery+"\"EVENT_ID\",\"EVENT_DATE\",\"IS_POST_TRANSACTION\",\"PAYEE_CODE\",\"CHANNEL\",\"PAYER_ACCT_ID\",\"PAYER_NAME\",\"AADHAR_NO\",\"DEVICE_ID\",\"PAYEE_ACCT_ID\",\"PAYER_IFSC_CODE\",\"AGENT_CODE\",\"TRAN_SUB_CATEGORY\",\"ACCOUNT_TYPE\",\"TRAN_ID\",\"IS_POOL_ACCT\",\"TRAN_AMT\",\"ACCOUNT_ID\",\"CUST_NAME\",\"PAYEE_NAME\",\"BRANCH_ID\",\"SENDER_VPA\",\"TRAN_CRNCY_CODE\",\"TRAN_RMKS\",\"TERMINAL_ID\",\"ACCT_NAME\",\"CUST_CARD_ID\",\"PAYEE_IFSC_CODE\",\"AVL_BAL\",\"PAYEE_ID\",\"PAYER_MOB_NO\",\"IS_REC_POOL_ACCT\",\"CORPORATE_ID\",\"COUNTRY_CODE\",\"CITY_CODE\",\"SUCC_FAIL_FLG\",\"PAYEE_NICK_NAME\",\"LAST_CUST_INDUCED_DT\",\"CUST_MOBILE\",\"TRAN_CATEGORY\",\"PAYER_NICK_NAME\",\"PAYER_CODE\",\"RECEIVER_VPA\",\"KVB_CUSTOMER\",\"USER_ID\",\"CUST_ID\",\"AUTH_TYPE\",\"PAYER_ID\",\"NETWORK_TYPE\",\"IP_ADDRESS\",\"TRAN_TYPE\",\"MOBILE_NO\",\"MCC_ID\",\"PAYEE_MOB_NO\",\"PART_TRAN_TYPE\") values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case SQLSERVER:
        insertQuery=insertQuery+"[EVENT_ID],[EVENT_DATE],[IS_POST_TRANSACTION],[PAYEE_CODE],[CHANNEL],[PAYER_ACCT_ID],[PAYER_NAME],[AADHAR_NO],[DEVICE_ID],[PAYEE_ACCT_ID],[PAYER_IFSC_CODE],[AGENT_CODE],[TRAN_SUB_CATEGORY],[ACCOUNT_TYPE],[TRAN_ID],[IS_POOL_ACCT],[TRAN_AMT],[ACCOUNT_ID],[CUST_NAME],[PAYEE_NAME],[BRANCH_ID],[SENDER_VPA],[TRAN_CRNCY_CODE],[TRAN_RMKS],[TERMINAL_ID],[ACCT_NAME],[CUST_CARD_ID],[PAYEE_IFSC_CODE],[AVL_BAL],[PAYEE_ID],[PAYER_MOB_NO],[IS_REC_POOL_ACCT],[CORPORATE_ID],[COUNTRY_CODE],[CITY_CODE],[SUCC_FAIL_FLG],[PAYEE_NICK_NAME],[LAST_CUST_INDUCED_DT],[CUST_MOBILE],[TRAN_CATEGORY],[PAYER_NICK_NAME],[PAYER_CODE],[RECEIVER_VPA],[KVB_CUSTOMER],[USER_ID],[CUST_ID],[AUTH_TYPE],[PAYER_ID],[NETWORK_TYPE],[IP_ADDRESS],[TRAN_TYPE],[MOBILE_NO],[MCC_ID],[PAYEE_MOB_NO],[PART_TRAN_TYPE]) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case MYSQL:
        insertQuery=insertQuery+"`EVENT_ID`,`EVENT_DATE`, `IS_POST_TRANSACTION`,`PAYEE_CODE`,`CHANNEL`,`PAYER_ACCT_ID`,`PAYER_NAME`,`AADHAR_NO`,`DEVICE_ID`,`PAYEE_ACCT_ID`,`PAYER_IFSC_CODE`,`AGENT_CODE`,`TRAN_SUB_CATEGORY`,`ACCOUNT_TYPE`,`TRAN_ID`,`IS_POOL_ACCT`,`TRAN_AMT`,`ACCOUNT_ID`,`CUST_NAME`,`PAYEE_NAME`,`BRANCH_ID`,`SENDER_VPA`,`TRAN_CRNCY_CODE`,`TRAN_RMKS`,`TERMINAL_ID`,`ACCT_NAME`,`CUST_CARD_ID`,`PAYEE_IFSC_CODE`,`AVL_BAL`,`PAYEE_ID`,`PAYER_MOB_NO`,`IS_REC_POOL_ACCT`,`CORPORATE_ID`,`COUNTRY_CODE`,`CITY_CODE`,`SUCC_FAIL_FLG`,`PAYEE_NICK_NAME`,`LAST_CUST_INDUCED_DT`,`CUST_MOBILE`,`TRAN_CATEGORY`,`PAYER_NICK_NAME`,`PAYER_CODE`,`RECEIVER_VPA`,`KVB_CUSTOMER`,`USER_ID`,`CUST_ID`,`AUTH_TYPE`,`PAYER_ID`,`NETWORK_TYPE`,`IP_ADDRESS`,`TRAN_TYPE`,`MOBILE_NO`,`MCC_ID`,`PAYEE_MOB_NO`,`PART_TRAN_TYPE`) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
 }
  return insertQuery;
}

private Put insert(Put put,String name, String value){
 byte[] cf = Bytes.toBytes("EVENT");
 byte[] nameBytes = Bytes.toBytes(name);
 byte[] valueBytes = value == null ? null : Bytes.toBytes(value);
 return put.addColumn(cf,nameBytes,valueBytes);
 }

 private String getColumnValue(Result rs , String cName){
     byte[] cf = Bytes.toBytes("EVENT");
     byte[] name = Bytes.toBytes(cName);
     byte [] value = rs.getValue(cf,name);
     return value == null ? "" : Bytes.toString(value);
 }
}

