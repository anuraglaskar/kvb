// -- ASSISTED CODE --
package cxps.events;

import clari5.platform.dbcon.CxConnection;
import clari5.platform.dbcon.DbTypeEnum;
import clari5.trace.ConnectionWrapper;
import clari5.trace.ConnectionWrapperException;
import clari5.trace.mappers.EventMapper;
import cxps.apex.shared.IWSEvent;
import cxps.noesis.core.EventHelper;

import com.google.protobuf.ServiceException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;


import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

public class NFT_IbpwdcngEventMapper extends EventMapper<NFT_IbpwdcngEvent> {

public NFT_IbpwdcngEventMapper () {}

@Override
public void save(ConnectionWrapper connectionWrapper, List<NFT_IbpwdcngEvent> list) throws ConnectionWrapperException {
  switch (connectionWrapper.getType()){
    case HBASE:
        saveInHbase(connectionWrapper.getHbaseConnection(),list);
        break;
    case RDBMS:
        saveInRDBMS(connectionWrapper.getCxConnection(),list);
        break;
    }
}


@Override
public List<IWSEvent> getAllEvents(ConnectionWrapper connectionWrapper, Set<String> set) throws ConnectionWrapperException {
   List<IWSEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      allevents = getAllEventsFromHbase(connectionWrapper.getHbaseConnection(),set);
      return allevents;
    case RDBMS:
      allevents = getAllEventsFromRDBMS(connectionWrapper.getCxConnection(),set);
      return allevents;
   }
   return null;
}

@Override
public List<NFT_IbpwdcngEvent> getAllEventsFromArchive(ConnectionWrapper connectionWrapper, Set<String> set, Date from, Date to) throws ConnectionWrapperException {
   List<NFT_IbpwdcngEvent> allevents;
   switch (connectionWrapper.getType()){
    case HBASE:
      return new ArrayList<>();
    case RDBMS:
      allevents = getArchivedEventsFromRDBMS(connectionWrapper.getCxConnection(),set, from, to);
      return allevents;
   }
   return new ArrayList<>();
}


public void saveInRDBMS(CxConnection con,  List<NFT_IbpwdcngEvent> eventList) throws ConnectionWrapperException {
 if(eventList == null) return;
 if(eventList.isEmpty()) return;
 if(!NFT_IbpwdcngEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

 try (PreparedStatement preparedStatement = con.prepareStatement(this.saveQuery(con.getDbType()))) {
        for (NFT_IbpwdcngEvent obj : eventList) {
            int i = 1;
            preparedStatement.setString(i++,obj.getEventId());
            preparedStatement.setTimestamp(i++,obj.getEventDate());
            preparedStatement.setString(i++, obj.isPostTransaction() ? "Y" : "N");
            preparedStatement.setString(i++, obj.getCountry());
            preparedStatement.setString(i++, obj.getCorporateId());
            preparedStatement.setString(i++, obj.getIpCity());
            preparedStatement.setString(i++, obj.getCngMode());
            preparedStatement.setString(i++, obj.getAccountId());
            preparedStatement.setString(i++, obj.getUserId());
            preparedStatement.setString(i++, obj.getErrorDesc());
            preparedStatement.setString(i++, obj.getChannel());
            preparedStatement.setString(i++, obj.getSuccFailFlg());
            preparedStatement.setString(i++, obj.getErrorCode());
            preparedStatement.setString(i++, obj.getIpCountry());
            preparedStatement.setString(i++, obj.getCustId());
            preparedStatement.setString(i++, obj.getType());
            preparedStatement.setString(i++, obj.getDeviceId());
            preparedStatement.setString(i++, obj.getResetUsing());
            preparedStatement.setString(i++, obj.getAddrNetwork());
            preparedStatement.setString(i++, obj.getDeviceType());
            preparedStatement.setString(i++, obj.getMobileNo());

            preparedStatement.addBatch();
        }
        preparedStatement.executeBatch();
    }catch (Exception e) {
        System.out.println("(RDBMS) Exception in saving data in [EVENT_NFT_IBPWDCNG]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    }
}

public void saveInHbase(Connection connection, List<NFT_IbpwdcngEvent> eventList) throws ConnectionWrapperException {
    if(eventList == null) return;
    if(eventList.isEmpty()) return;
    if(!NFT_IbpwdcngEvent.class.isAssignableFrom(eventList.get(0).getClass())) return;

    Table table = null;
    List <Put> putList = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_NFT_IBPWDCNG"));
        putList = new ArrayList<>();

        for (NFT_IbpwdcngEvent obj : eventList){

            Put p = new Put(Bytes.toBytes(obj.getEventId()));
            Timestamp ts = obj.getEventDate();
            p = this.insert(p,"EVENT_ID",obj.getEventId());
            p = this.insert(p,"EVENT_DATE", ts == null ? null : String.valueOf(ts));
            p = this.insert(p,"IS_POST_TRANSACTION",obj.isPostTransaction() ? "Y" : "N");
            p = this.insert(p, "COUNTRY",  obj.getCountry());
            p = this.insert(p, "CORPORATE_ID",  obj.getCorporateId());
            p = this.insert(p, "IP_CITY",  obj.getIpCity());
            p = this.insert(p, "CNG_MODE",  obj.getCngMode());
            p = this.insert(p, "ACCOUNT_ID",  obj.getAccountId());
            p = this.insert(p, "USER_ID",  obj.getUserId());
            p = this.insert(p, "ERROR_DESC",  obj.getErrorDesc());
            p = this.insert(p, "CHANNEL",  obj.getChannel());
            p = this.insert(p, "SUCC_FAIL_FLG",  obj.getSuccFailFlg());
            p = this.insert(p, "ERROR_CODE",  obj.getErrorCode());
            p = this.insert(p, "IP_COUNTRY",  obj.getIpCountry());
            p = this.insert(p, "CUST_ID",  obj.getCustId());
            p = this.insert(p, "TYPE",  obj.getType());
            p = this.insert(p, "DEVICE_ID",  obj.getDeviceId());
            p = this.insert(p, "RESET_USING",  obj.getResetUsing());
            p = this.insert(p, "ADDR_NETWORK",  obj.getAddrNetwork());
            p = this.insert(p, "DEVICE_TYPE",  obj.getDeviceType());
            p = this.insert(p, "MOBILE_NO",  obj.getMobileNo());
            putList.add(p);
        }
        table.put(putList);

    } catch (IOException e) {
        Admin admin = null;
        try {
            //if table does not exists, create table and insert again.
            admin = connection.getAdmin();
            HTableDescriptor hTableDescriptor = new HTableDescriptor(TableName.valueOf("EVENT_NFT_IBPWDCNG"));
            hTableDescriptor.addFamily(new HColumnDescriptor("EVENT"));
            admin.createTable(hTableDescriptor);
            table.put(putList);
        } catch (Exception e1) {
            System.out.println("(HBASE) Exception in saving data in [EVENT_NFT_IBPWDCNG]: "+e.getMessage());
            throw new ConnectionWrapperException(e1.getMessage(), e1.getCause());
        } finally {
            try {
                if (admin != null)
                    admin.close();
                if (table != null)
                    table.close();
            } catch (IOException e1) {
                System.out.println("(HBASE) Exception for [EVENT_NFT_IBPWDCNG]: "+e.getMessage());
                throw new ConnectionWrapperException(e.getMessage(), e.getCause());
            }
        }
    }

}


public List<IWSEvent> getAllEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds) throws ConnectionWrapperException {
 List<IWSEvent> events = new ArrayList<>();
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");
 try{
 cxConnection.query(this.fetchQuery(cxConnection.getDbType()) + " where event_id in ("+ sb.toString() + ")", rs -> {
 while (rs.next()) {
    NFT_IbpwdcngEvent obj = new NFT_IbpwdcngEvent();
    obj.setEventId(rs.getString("EVENT_ID"));
    obj.setEventDate(rs.getTimestamp("EVENT_DATE"));
    obj.setEventTS(obj.getEventDate());
    obj.setPostTransaction(rs.getString("IS_POST_TRANSACTION").equals("Y"));

    obj.setCountry(rs.getString("COUNTRY"));
    obj.setCorporateId(rs.getString("CORPORATE_ID"));
    obj.setIpCity(rs.getString("IP_CITY"));
    obj.setCngMode(rs.getString("CNG_MODE"));
    obj.setAccountId(rs.getString("ACCOUNT_ID"));
    obj.setUserId(rs.getString("USER_ID"));
    obj.setErrorDesc(rs.getString("ERROR_DESC"));
    obj.setChannel(rs.getString("CHANNEL"));
    obj.setSuccFailFlg(rs.getString("SUCC_FAIL_FLG"));
    obj.setErrorCode(rs.getString("ERROR_CODE"));
    obj.setIpCountry(rs.getString("IP_COUNTRY"));
    obj.setCustId(rs.getString("CUST_ID"));
    obj.setType(rs.getString("TYPE"));
    obj.setDeviceId(rs.getString("DEVICE_ID"));
    obj.setResetUsing(rs.getString("RESET_USING"));
    obj.setAddrNetwork(rs.getString("ADDR_NETWORK"));
    obj.setDeviceType(rs.getString("DEVICE_TYPE"));
    obj.setMobileNo(rs.getString("MOBILE_NO"));

    events.add(obj);
   }});
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting data from [EVENT_NFT_IBPWDCNG]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<NFT_IbpwdcngEvent> getArchivedEventsFromRDBMS(CxConnection cxConnection, Set<String> eventIds, Date from, Date to) throws ConnectionWrapperException {
 StringBuilder sb = new StringBuilder("");
 for (String eventId : eventIds) {
     sb.append("'").append(eventId).append("', ");
 }
 sb.replace(sb.lastIndexOf(","), sb.length(), "");

 List<NFT_IbpwdcngEvent> events;
 NFT_IbpwdcngEvent obj = new NFT_IbpwdcngEvent(cxConnection, from, to);
 try{
     events = obj.t.loadAllFilter(cxConnection," where event_id in ("+ sb.toString() + ")", null, null);
   } catch (Exception e) {
      System.out.println("(RDBMS) Exception in getting archived data from ["+obj.t.getTableName()+"]: "+e.getMessage());
      throw new ConnectionWrapperException(e.getMessage(), e.getCause());
   }
 return events;
}

public List<IWSEvent> getAllEventsFromHbase(Connection connection, Set<String> eventIds) throws ConnectionWrapperException {

    List<IWSEvent> events = new ArrayList<>();
    List<Get> getList = new ArrayList<>();
    Table table = null;
    try {
        table = connection.getTable(TableName.valueOf("EVENT_NFT_IBPWDCNG"));
        for (String id : eventIds){
            Get g = new Get(Bytes.toBytes(id));
            getList.add(g);
        }
        Result[] results = table.get(getList);
        for (Result rs : results){
            NFT_IbpwdcngEvent obj = new NFT_IbpwdcngEvent();
            obj.setEventId(getColumnValue(rs, "EVENT_ID"));
            String ts = getColumnValue(rs, "EVENT_DATE");
            obj.setEventDate((ts == null || ts.trim().equalsIgnoreCase("")) ? null : new Timestamp(Long.parseLong(ts.trim())));
            obj.setEventTS(obj.getEventDate());
            obj.setPostTransaction(getColumnValue(rs,"IS_POST_TRANSACTION").equals("Y"));

            obj.setCountry(getColumnValue(rs, "COUNTRY"));
            obj.setCorporateId(getColumnValue(rs, "CORPORATE_ID"));
            obj.setIpCity(getColumnValue(rs, "IP_CITY"));
            obj.setCngMode(getColumnValue(rs, "CNG_MODE"));
            obj.setAccountId(getColumnValue(rs, "ACCOUNT_ID"));
            obj.setUserId(getColumnValue(rs, "USER_ID"));
            obj.setErrorDesc(getColumnValue(rs, "ERROR_DESC"));
            obj.setChannel(getColumnValue(rs, "CHANNEL"));
            obj.setSuccFailFlg(getColumnValue(rs, "SUCC_FAIL_FLG"));
            obj.setErrorCode(getColumnValue(rs, "ERROR_CODE"));
            obj.setIpCountry(getColumnValue(rs, "IP_COUNTRY"));
            obj.setCustId(getColumnValue(rs, "CUST_ID"));
            obj.setType(getColumnValue(rs, "TYPE"));
            obj.setDeviceId(getColumnValue(rs, "DEVICE_ID"));
            obj.setResetUsing(getColumnValue(rs, "RESET_USING"));
            obj.setAddrNetwork(getColumnValue(rs, "ADDR_NETWORK"));
            obj.setDeviceType(getColumnValue(rs, "DEVICE_TYPE"));
            obj.setMobileNo(getColumnValue(rs, "MOBILE_NO"));

            events.add(obj);
        }
        return events;

    } catch (IOException e) {
        System.out.println("(HBASE) Exception in getting data from [EVENT_NFT_IBPWDCNG]: "+e.getMessage());
        throw new ConnectionWrapperException(e.getMessage(), e.getCause());
    } finally {
        if (table != null) try {
            table.close();
        } catch (IOException e) {
            System.out.println("(HBASE) Exception in getting data from [EVENT_NFT_IBPWDCNG]: "+e.getMessage());
            throw new ConnectionWrapperException(e.getMessage(), e.getCause());
        }
    }
}


private String fetchQuery(DbTypeEnum dbTypeEnum) {
 switch (dbTypeEnum) {
  case ORACLE:
      return "SELECT \"EVENT_ID\", \"EVENT_DATE\",\"IS_POST_TRANSACTION\", \"COUNTRY\",\"CORPORATE_ID\",\"IP_CITY\",\"CNG_MODE\",\"ACCOUNT_ID\",\"USER_ID\",\"ERROR_DESC\",\"CHANNEL\",\"SUCC_FAIL_FLG\",\"ERROR_CODE\",\"IP_COUNTRY\",\"CUST_ID\",\"TYPE\",\"DEVICE_ID\",\"RESET_USING\",\"ADDR_NETWORK\",\"DEVICE_TYPE\",\"MOBILE_NO\"" +
              " FROM EVENT_NFT_IBPWDCNG";
  case SQLSERVER:
      return "SELECT [EVENT_ID], [EVENT_DATE],[IS_POST_TRANSACTION], [COUNTRY],[CORPORATE_ID],[IP_CITY],[CNG_MODE],[ACCOUNT_ID],[USER_ID],[ERROR_DESC],[CHANNEL],[SUCC_FAIL_FLG],[ERROR_CODE],[IP_COUNTRY],[CUST_ID],[TYPE],[DEVICE_ID],[RESET_USING],[ADDR_NETWORK],[DEVICE_TYPE],[MOBILE_NO]" +
              " FROM EVENT_NFT_IBPWDCNG";
  case MYSQL:
      return "SELECT `EVENT_ID`, `EVENT_DATE`,`IS_POST_TRANSACTION`,`COUNTRY`,`CORPORATE_ID`,`IP_CITY`,`CNG_MODE`,`ACCOUNT_ID`,`USER_ID`,`ERROR_DESC`,`CHANNEL`,`SUCC_FAIL_FLG`,`ERROR_CODE`,`IP_COUNTRY`,`CUST_ID`,`TYPE`,`DEVICE_ID`,`RESET_USING`,`ADDR_NETWORK`,`DEVICE_TYPE`,`MOBILE_NO`" +
              " FROM EVENT_NFT_IBPWDCNG";
 }
  return null;
}

private String saveQuery(DbTypeEnum dbType) {

 String insertQuery="INSERT INTO EVENT_NFT_IBPWDCNG (";
 switch(dbType){
    case ORACLE:
        insertQuery=insertQuery+"\"EVENT_ID\",\"EVENT_DATE\",\"IS_POST_TRANSACTION\",\"COUNTRY\",\"CORPORATE_ID\",\"IP_CITY\",\"CNG_MODE\",\"ACCOUNT_ID\",\"USER_ID\",\"ERROR_DESC\",\"CHANNEL\",\"SUCC_FAIL_FLG\",\"ERROR_CODE\",\"IP_COUNTRY\",\"CUST_ID\",\"TYPE\",\"DEVICE_ID\",\"RESET_USING\",\"ADDR_NETWORK\",\"DEVICE_TYPE\",\"MOBILE_NO\") values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case SQLSERVER:
        insertQuery=insertQuery+"[EVENT_ID],[EVENT_DATE],[IS_POST_TRANSACTION],[COUNTRY],[CORPORATE_ID],[IP_CITY],[CNG_MODE],[ACCOUNT_ID],[USER_ID],[ERROR_DESC],[CHANNEL],[SUCC_FAIL_FLG],[ERROR_CODE],[IP_COUNTRY],[CUST_ID],[TYPE],[DEVICE_ID],[RESET_USING],[ADDR_NETWORK],[DEVICE_TYPE],[MOBILE_NO]) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
    case MYSQL:
        insertQuery=insertQuery+"`EVENT_ID`,`EVENT_DATE`, `IS_POST_TRANSACTION`,`COUNTRY`,`CORPORATE_ID`,`IP_CITY`,`CNG_MODE`,`ACCOUNT_ID`,`USER_ID`,`ERROR_DESC`,`CHANNEL`,`SUCC_FAIL_FLG`,`ERROR_CODE`,`IP_COUNTRY`,`CUST_ID`,`TYPE`,`DEVICE_ID`,`RESET_USING`,`ADDR_NETWORK`,`DEVICE_TYPE`,`MOBILE_NO`) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        break;
 }
  return insertQuery;
}

private Put insert(Put put,String name, String value){
 byte[] cf = Bytes.toBytes("EVENT");
 byte[] nameBytes = Bytes.toBytes(name);
 byte[] valueBytes = value == null ? null : Bytes.toBytes(value);
 return put.addColumn(cf,nameBytes,valueBytes);
 }

 private String getColumnValue(Result rs , String cName){
     byte[] cf = Bytes.toBytes("EVENT");
     byte[] name = Bytes.toBytes(cName);
     byte [] value = rs.getValue(cf,name);
     return value == null ? "" : Bytes.toString(value);
 }
}

