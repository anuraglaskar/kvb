cxps.noesis.glossary.entity.pool-account {
       db-name = POOL_ACCOUNT
       generate = false
       db_column_quoted = true

       tablespace = CXPS_USERS
                       attributes:[
                               { name: ACCOUNT_ID, type="string:20", key=true}
                               ]
                       }