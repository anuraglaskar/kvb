cxps.events.event.nft-payeereg{
table-name : EVENT_NFT_PAYEEREG
event-mnemonic : PG
        workspaces : {
        CUSTOMER : "cust-id"
	    ACCOUNT : "account-id"
        NONCUSTOMER : "payee-account-id + payee-ifsc-code"

}
event-attributes : {
channel:{type:"string:20"}
user-id:{ type:"string:20"}
cust-id:{ type:"string:20", fr : true}
mobile-no:{ type:"string:20"}
account-id:{ type:"string:20", fr : true}
device-id:{ type:"string:20"}
addr-network:{ type:"string:20"}
country:{ type:"string:20"}
succ-fail-flg:{ type:"string:2"}
error-code:{ type:"string:20"}
error-desc:{ type:"string:50"}
payee-type:{ type:"string:20"}
payee-id:{ type:"string:20"}
payee-name:{ type:"string:50"}
payee-nickname:{ type:"string:20"}
payee-accounttype:{ type:"string:20"}
payee-ifsc-code:{ type:"string:20", fr : true}
payee-account-id:{ type:"string:20", fr : true}
payee-account-name:{ type:"string:50"}
payee-bank-name:{ type:"string:50"}
payee-bank-id:{ type:"string:20"}
payee-branch-name:{ type:"string:50"}
payee-branch-id:{ type:"string:20"}
payee-status:{ type:"string:20"}
network-type:{ type:"string:20"}
mfa-type:{ type:"string:20"}
payee-limit:{ type:"number:20,3"}
vp-id:{ type:"string:20"}
}
}
