#!/usr/bin/env groovy

import groovy.json.JsonBuilder

/**
 *  Fetches the svn version
 */
String getVersion() {

    StringBuilder retVal = new StringBuilder()

    def env = System.env.collect { k,v -> "$k=$v" }
    def v = "svnversion -c".execute(env, new File(System.getenv("CXPS_WORKSPACE"))).text

    def arr = v.split(":")

    if (arr.length > 1)     v = arr[1]
    else                    v = arr[0]

    for (char ch : v) {
        if (ch.isDigit()) retVal.append(ch)
    }

    return retVal.toString()
}

def installDir = System.getenv("CL5_WS_REP")
if (!installDir) {
    System.err.print("Error: Unable to find the rep base [CL5_WS_REP]")
    System.exit(1)
}

def temp = new File(installDir)
if(!temp.exists() || !temp.isDirectory()) {
    println "Error: Installation directory [${installDir}] not found!"
}

temp = System.getenv("CL5_REL_NAME")
String maj, min
if (temp == "SNAPSHOT") {
    maj = "0" 
    min = version
} else if (temp.contains(".")) {
    (maj, min) = temp.split("[.]")
} else {
    maj = "0" 
    min = temp
}

def map = [
        "tag" : "EFM Product dev installer",
        "major-version" : "${maj}",
        "minor-version" : "${min}"
] as Map<String, ?>

def app = [:]
temp = new File("${installDir}/rep/apps")
temp.listFiles().each {
    if(it.directory) {
        System.err.println("Error: Found ${it.name} app, archive file expected")
        System.exit(1)
    }

    def appname = it.name.substring(0, it.name.lastIndexOf("."))
    def out = new File("${installDir}/meta/${appname}.revision")
    if(!out){
        return false;
    }
    app[appname] = [
            "revision" : out.text,
            "compression" : "ZIP"
    ]
}
map.put("apps",app)

def containers = [:]
temp = new File("${installDir}/deps/container")
temp.eachDir(){
    def cont = [:]

    new File(it.getAbsolutePath()).eachDir(){ f->
        new File(f.getAbsolutePath()).listFiles().each{
            String compression = it.getName().substring(it.getName().lastIndexOf(".")+1)
            cont.put("version", f.getName());
            cont.put("compression", compression.toUpperCase())
        }
    }
    containers.put(it.getName(), cont)
}
map.put("container", containers)


def webservers = [:]
int count = 0;
temp = new File("${installDir}/deps/web-server")
if (temp.exists()) {
    temp.eachDir(){
        webservers.put("name", it.getName())
        new File(it.getAbsolutePath()).eachDir(){ f->
            if(count<1) {
                webservers.put("version", f.getName())
                webservers.put("compression", "TGZ")
                count++
            } else {
                System.err.println("Error: Multiple webservers are not allowed")
                System.exit(1)
            }
        }
    }
    map.put("web-server", webservers)
}

def dependencies = [:]
temp = new File("${installDir}/deps/others")
if (temp.exists()) {
    temp.eachDir(){
        def deps = [:]
        new File(it.getAbsolutePath()).eachDir(){f->
            new File(f.getAbsolutePath()).listFiles().each {
                String compression = it.getName().substring(it.getName().lastIndexOf(".")+1)
                deps.put("version", f.getName());
                deps.put("compression", compression.toUpperCase())
            }
        }
        dependencies.put(it.getName(),deps)
    }
    map.put("dependency", dependencies)
}

File packageMeta = new File("${installDir}/package-meta.conf")
if(packageMeta.exists()) packageMeta.delete()
packageMeta << new JsonBuilder(map).toPrettyString()
