from __future__ import print_function
 
# Special END separator
END = '0e8ed89a-47ba-4cdb-938e-b8af8e084d5c'
 
# Text attributes
ALL_OFF = '\033[0m'
BOLD = '\033[1m'
UNDERSCORE = '\033[4m'
BLINK = '\033[5m'
REVERSE = '\033[7m'
CONCEALED = '\033[7m'
 
# Foreground colors
FG_BLACK = '\033[30m'
FG_RED = '\033[31m'
FG_GREEN = '\033[32m'
FG_YELLOW = '\033[33m'
FG_BLUE = '\033[34m'
FG_MAGENTA = '\033[35m'
FG_CYAN = '\033[36m'
FG_WHITE = '\033[37m'
 
# Background colors
BG_BLACK = '\033[40m'
BG_RED = '\033[41m'
BG_GREEN = '\033[42m'
BG_YELLOW = '\033[43m'
BG_BLUE = '\033[44m'
BG_MAGENTA = '\033[45m'
BG_CYAN = '\033[46m'
BG_WHITE = '\033[47m'

class ColorText():
	'''
	Context manager for pretty terminal prints
	'''

	def __init__(self, *attr):
		self.attributes = attr
 
	def __enter__(self):
		return self
 
	def __exit__(self, type, value, traceback):
		pass
 
	def write(self, msg):
		style = ''.join(self.attributes)
		print('{0}{1}{2}'.format(style, msg.replace(END, ALL_OFF + style), ALL_OFF))
 
 
if __name__ == '__main__':
 
	with ColorText(FG_RED) as out:
		out.write('This is a test in RED')
 
	with ColorText(FG_BLUE) as out:
		out.write('This is a test in BLUE')
 
	with ColorText(BOLD, FG_GREEN) as out:
		out.write('This is a bold text in green')
 
	with ColorText(BOLD, BG_GREEN) as out:
		out.write('This is a text with green background')
 
	with ColorText(BOLD, FG_BLUE, BG_WHITE) as out:
		out.write('This is blue text with white background')

	with ColorText(FG_GREEN) as out:
		out.write('This is a green text with ' + BOLD + 'bold' + END + ' text included')
 
	with ColorText() as out:
		out.write(BOLD + 'Use this' + END + ' even with ' + BOLD + FG_RED + 'no parameters' + END + ' in the with statement') 
